---
layout: ../layouts/AirThemeLayout.astro
title: "JS, le minimum pour commencer à en faire"
author: "Antoine Haas"
date: "26 octobre 2022"
---

## Rappels sur les types et la déclaration de variables

### Instancier une variable (rappels)
Pour instancier une variable il est possible d'utiliser les mots-clés *let*, *const* et *var* :
* *let* pour instancier une variable qui pourra être modifiée par la suite
* *const* pour instancier une variable qui ne pourra pas être modifiée
* *var*, la manière la plus ancienne d'instancier une variable. Il est recommandé d'utliser *let* et *const*, hormis en cas de problème de compatibilité (pour des versions de Javascript assez ancienne, pré ES6).

```js
var var1 = "";
let var2 = "";
const var3 = "";
```

Pour plus des informations plus détaillées : [https://medium.com/geekculture/javascript-declaring-and-initializing-variables-how-data-is-stored-and-accessed-in-javascript-2936f4d69ce0](https://medium.com/geekculture/javascript-declaring-and-initializing-variables-how-data-is-stored-and-accessed-in-javascript-2936f4d69ce0)

### Les types de variables (rappels)
Documentation complète : [https://developer.mozilla.org/fr/docs/Web/JavaScript/Data_structures](https://developer.mozilla.org/fr/docs/Web/JavaScript/Data_structures)

* *boolean* (true et false)
* *null*
* *undefined*
* *number* (-10, -9, -8, ..., 1, 2, 3, ..., 3.14, 3.15, 3.16, ...)
* *string* ("a", "abc", "Lorem Ipsum", \`string interpolation\`, ...)
* *array* (\[ \], \['toto', '...'\]), ...)

## Rappels sur les évènements

## Les conditions
Comme dans tous langages, Javascript possède des conditions. Celles-ci sont plutôt semblables aux langages qui tirent leur inspiration du *C*.

### Les opérateurs logiques
Quatres opérateurs logiques existent en JS 

1. Le **|| (OR)**
2. Le **&& (AND)**
3. Le **! (NOT)**
4. Le **?? (Nullish coalescing operator)

Le *||*, *&&*, et le *!* sont similaires aux autres langages dans le fonctionnement.

Le *??* quant à lui permet d'attribuer une valeur par défaut si la valeur précédent le *??* est de type *null* ou *undefined*.

```js
const nullVal = null;
const notNullVal = 'not null val';

console.log(nullVal ?? 'default value'); // default value
console.log(notNullVal ?? 'default value'); // not null val
```

* [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator)
* [https://javascript.info/logical-operators](https://javascript.info/logical-operators)

### Les opérateurs de comparaisons
JS étant un langage faiblement typé il existe deux types de comparaisons :
* Les comparaisons standards
* Les comparaisons strictes

Les comparaisons standards en Javascript vont tenter, avant d'effectuer la comparaison, de convertir les deux opérandes si celles-ci ne sont pas du même type. Voici quelques exemples :

* *0 == false* => *true*
* *\[\] == false* => *true*
* *"" == false* => *true*

Ces exemples ci-dessus impliquent donc :
* *0 == ""* => *true*
* *'' == \[\]* => *true*
* ...

Et quelques derniers exemples de conversion :
* *3 == '3'* => *true*
* *\[3\] == '3'* => *true*
* 3 == true => *true*
* *3 == "var3"* => *false*

Voici tous les opérateurs de comparaisons standards :
* *==*
* *!=*
* *<*
* *>*
* *<=*
* *>=*

Les opérateurs de comparaisons restant sont des opérateurs dits strictes.
**Pour que la condition soit vraie il est nécessaire que les valeurs comparées soient égales et que le type soit similaire.**

Seules deux opérateurs strictes existent :
* *===*
* *!==*

**Attention : Si aucun opérateur de comparaison n'est utilisé alors la condition effectuera une comparaison standard, avec conversion de valeur vers le type booléen !**

[https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Expressions_and_Operators#op%C3%A9rateurs_de_comparaison](https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Expressions_and_Operators#op%C3%A9rateurs_de_comparaison)

### Le if traditionnel
```js
const condition1 = 1;
const condition2 = '1';
const condition3 = "toto";

if(condition1 == '' || condition2) {
  console.log('if true');
}
else if(condition3) {
  console.log('else if true');
}
else {
  console.log('The cake is a lie');
}
```
,
### Les conditions ternaires
```js
const condition1 = true;
const value = !condition1 ? 'valeur si vrai' : 'valeur si faux';
```

## Les boucles
Ils existent de nombreuses manières d'itérer sur un tableau, en voici quelques exemples :

```js
const myArray = [1, 2, 3, 4, 5, 6]
let i = 0;

for(i = 0; i < myArray.length; i++) {
  console.log(myArray[i]);
}

i = 0
do {
  console.log(myArray[i++]);
} while(i < myArray.length);

i=0
while(i < myArray.length) {
  console.log(myArray[i]);
  i++;
}

for(let current of myArray) {
    console.log(current);
}
```

Il est également possible d'itérer sur et d'intéragir avec les tableaux en utilisant les paradigmes de la programmation fonctionnelle tels que les fonctions *map*, *filter*, *reduce*, *find* ou encore d'autres méthodes [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)

```js
const incrementedArray = [1, 2, 3, 4, 5, 6].map((v) => v+1) // [2, 3, 4, 5, 6, 7]
const onlyEven = incrementedArray.filter((v) => v%2 === 0); // [2, 4, 6]
const evenSum = onlyEven.reduce((accumulator, current) => { return accumulator + current; }, 0) // 12
const firstGreaterThan10 = [1, 4, 9, 20, 18].find(function(v) { return v > 10; }) // 20
```

## Les fonctions
```js
// attribuer une fonction à une variable (= similaire à la déclaration de fonction)
const add = (i, j) => (i + j)

const sub = (i, j) => {
  return i - j;
}
const mult = function(i, j) {
  return i * j;
}

// déclarer une fonction
function div(i, j) {
  return i / j;
}

console.log(add(1, 2), sub(3, 2), mult(4, 6), div(15, 3));
```

### Les fonctions anonymes
Les fonctions anonymes sont simplement des fonctions qui n'ont pas de nom. Dans l'exemple précédent **les fonctions** attribuées aux variables *add*, *sub* et *mult* sont toutes trois des fonctions anonymes car elles n'ont pas de nom.

### Les callbacks
Une callback est une fonction *b* passée en paramètre d'une autre fonction *a* et qui sera appelée lorsque le code de la fonction *a* aura été exécuté.

Ce méchanisme est très utilisé en Javascript car le langage est par nature asynchrone grâce (ou à cause) de la omniprésence des évènements qui sont chacuns asynchrones et qui au moment de la réalisation de l'évènement, doit à la fin, exécuter une action (en l'occurence, la fonction passée en paramètre).

Voici un exemple de callback :

```js
function myAlert() {
  alert("Clicked");
}

const el = document.querySelector("...")
el.addEventListener("click", () => { alert("Clicked!"); })
// or
el.addEventListener("click", myAlert);
```

Le plus souvent, ce soit des fonctions anonymes qui sont utilisées pour les callbacks même s'il est également possible d'appeler une fonction nommée ou une variable qui fait référence à une fonction.

## Les objets
La programmation orienté objet est hors cadre de cette UE, mais nous allons tout de même aborder les rudiments qui permettront d'utiliser les objets Javascript en tant que structure de données.

### Légère digression sur le JSON
Le JSON est la notation textuelle d'un objet Javascript (*JavaScript Object Notation*). Il est sous la forme de clé valeur et est très utilisé de nos jours pour l'échange de données entre applications ou la sauvegarde de données. Il est de la forme :

```json
{
  "key": "value",
  "number": 1,
  "bool": true,
  "json": {
    "text": "text"
  },
  "array": [1, 2, 3, 4],
}
```

Il peut être représenté sous forme d'objet ou sous forme de dictionnaire **(en Javascript, les deux sont la même notion)**.

### Les bases d'un objet
**Les attributs :**
Un objet contient des attributs qui lui sont propres. Un attribut est simplement, pour vulgariser, une variable propre à l'instance d'un objet. 
Sa valeur est donc forcément l'un des types cité dans la section sur les types **ou** un autre objet. 

**Les méthodes :**
Un objet contient également des méthodes. Une méthode est, pour vulgariser, une fonction propre à l'instance d'un objet.

**Faire appel aux attributs ou aux méthodes :**
Pour pouvoir faire appel aux attributs/méthodes de l'instance courante dans l'objet il est nécessaire d'utilisé le mot-clé *this*, tel que `this.monAttribut = this.monAutreAttribut` ou `this.maFonction()`. Autrement, en dehors de l'objet, il est possible de les appeler en utilisant le code suivant `monObjet.maFonction()` ou `monObjet.monAttribut`.

L'utilisation d'objet/dictionnaire n'est pas obligatoire, mais tout de même grandement recommandé car ils permettent de structurer plus simplement le code de l'application. 

### Mon premier objet
Pour créer un objet en Javascript il est possible de le faire de deux façons (non exclusives) :
* lors de la déclaration de l'objet
```js
// déclaration de l'objet
const myNewObject = {
  attr1: '',
  attr2: 0,
  attr3: [],
  function1: (str) => { alert(str) },
  function2: function() { console.log(this.attr2) }
}

// utilisation
myNewObject.function2() // 0
myNewObject.attr2 = 12
myNewObject.function2() // 12
myNewObject.attr2 = 20
myNewObject.function2() // 20

console.log(myNewObject.attr3) // []
myNewObject.attr3.push(3)
console.log(myNewObject.attr3) // [3]
```

* par la suite
```js
// déclaration de l'objet
const myNewObject = {}
myNewObject.attr1 = ''
myNewObject.attr2 = 0
myNewObject.attr3 = []
myNewObject.function1 = (str) => { alert(str) }
myNewObject.function2 = function() { console.log(this.attr2) }

// utilisation
myNewObject.function2() // 0
myNewObject.attr2 = 12
myNewObject.function2() // 12
myNewObject.attr2 = 20
myNewObject.function2() // 20

console.log(myNewObject.attr3) // []
myNewObject.attr3.push(3)
console.log(myNewObject.attr3) // [3]
```

### Cloner un objet ?
Pour éviter de dupliquer trop de code et se simplifier la vie, il est possible de créer un objet *template* et de créer une nouvelle instance de ce prototype au besoin.

```js
const myTemplateObject = {
  attr1: '',
  attr2: 0,
  attr3: [],
  function1: (str) => { alert(str) },
  function2: function() { console.log(this.attr2) }
}

const myNewObject = Object.create(myTemplateObject)
```

## Rappels (bis)
### Récupérer un élément et le modifier
```html
<html>
  <body>
    <div id="el1" class="elems"></div>
    <div class="elems"></div>
    <div class="elems"></div>
    <span class="elems"></span>
    
    <script type="text/javascript">
      // get an HTML elem by ID
      const el1 = document.getElementById('el1')
      // get HTML elems by classname
      const elems = document.getElementsByClassName('elems')
      // get HTML elem by tagname
      const divs = document.getElementsByTagName('div')
      
      // get HTML elem by CSS selector
      const alternative = document.querySelector('#el1')
      const alternative2 = document.querySelector('.elems')
      
      // add or remove a class
      alternative.classList.add("add-a-new-classe")
      alternative.classList.remove("add-a-new-classe")

    </script>
  </body>
</html>
```
Nous avons vu uniquement un petit nombre de fonctions et propriétés permettant de modifier un élémént, mais un grand nombre d'autres fonctions sont disponibles dans la documentation à cette adresse [https://developer.mozilla.org/en-US/docs/Web/API/Element](https://developer.mozilla.org/en-US/docs/Web/API/Element).

### Les évènements
```html
<html>
  <body>
    <button id="el1" class="btn" onclick="console.log('clicked btn 1')"></button>
    <button id="el2" class="btn"></button>
    
    <script type="text/javascript">
      const el2 = document.querySelector('#el1')
      el2.addEventListener("click", () => { alert("Clicked btn 2!"); })      
    </script>
  </body>
</html>
```

## Complément sur les évènements

### Les évènements les plus utilisés
La liste des évènements les plus souvent utilisés :

* *pointerdown/pointerup* (quand la souris/pointeur est relâché ou appuyé sur l'élément)
* *pointerover/pointerout* (quand la souris/pointeur rentre ou sort de l'élément) 
* *pointermove* (quand la souris/pointeur se déplace au sein de l'élément)
* *click*
* *dblclick*
* *contextmenu* (dans des applications complexes, comme Google Sheets par exemple, pour modifier le menu lors du clic droit de la souris)
* *change* (quand la valeur change et que le focus de l'élément est quitté)
* *input* (à chaque changement de valeur)
* *scroll* (au scroll de la page)

Les évènements de types *key* et *mouse* ne sont plus vraiment utilisés afin de priviligier le Responsive Design.

### Le bubbling
Dans Javascript, le trigger des évènements s'effectue sur l'élément le plus spécifique (le plus profond au sein du DOM) au plus global (parent par parent). 
Il est ainsi facile de connaître l'ordre d'exécution si plusieurs events sont présents sur les fils et les parents.

De manière générale pour les applications les plus simples et dans les rares cas où plusieurs évènements sont en concurrences, il ne devrait pas y avoir d'effet de bord ou de conflit (mais cela est somme toute possible).

Le plus simple est d'éviter ces conflits en structurant votre code afin qu'ils ne surviennent pas.

### Les actions par défaut
Sur certains évènements il existe des évènements par défaut. Par exemple lors du clic sur un lien `<a href="...">...</a>` le navigateur effectuera la redirection vers l'url précisé dans l'attribut *href*.

Pour empêcher le fonctionnement par défaut vous pouvez utiliser l'objet *event* passé en paramètre de votre event listener et appeler la méthode *preventDefault()*.

```html
<html>
  <body>
    <a href="google.com"></button>
    
    <script type="text/javascript">
      const a = document.querySelector('a')
      a.addEventListener("click", (e) => { 
        e.preventDefault()
        // do what you want
      })      
    </script>
  </body>
</html>
```

Pour des informations plus détaillées ou complémentaires vous pouvez vous rendre les liens ci-dessous :

* [https://javascript.info/events](https://javascript.info/events)
* [https://javascript.info/event-details](https://javascript.info/event-details)
* [https://javascript.info/forms-controls](https://javascript.info/forms-controls)