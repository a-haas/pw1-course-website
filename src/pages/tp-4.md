---
layout: ../layouts/AirThemeLayout.astro
title: "TP 4 - Du JS, du JS, du JS"
author: "Antoine Haas"
date: "26 octobre 2022"
---

## Exercice 1 : manipulation de tableaux

**Scénario : vous venez de recevoir une note de contrôle continu. Vous proposez d’aider vos enseignants à
analyser les résultats. Ils vous donnent les notes suivantes: 10, 15, 6, 14, 14, 13, 19, 10, 17 et 9.**

1. Dans votre script, créer un tableau avec ces valeurs.
2. Créer une fonction qui prend un argument pour afficher ce tableau dans votre document HTML.
Pour cela, vous parcourerez votre tableau avec une boucle for, puis dans un second temps, avec une
boucle while.
3. Modifier votre fonction en lui ajoutant un second argument. Ce second argument sera un booléen qui
permettra de dire si on veut afficher toutes les valeurs (true) ou uniquement les valeurs supérieures
ou égales à 15 (dans ce cas là le booléen aura la valeur false). Dans ce dernier cas, vous devez
utiliser une instruction vous permettant de sauter une itération.
4. Ajouter une conditionnelle permettant de tester le type du second argument de votre fonction :
comme Javascript ne fait pas de contrôle sur les types, un utilisateur aura vite fait de ne pas passer
un booléen en argument de votre fonction. Si le type n’est pas correct, alors afficher un message
(dans une boite d’alerte par exemple)

## Exercice 2 : Conversion d’unités
Vous allez créer un petit programme HTML/Javascript pour convertir des mesures d’une unité à une
autre : des pouces en centimètres (1 pouce = 2.54 cm) et inversement, ainsi que des livres en grammes
(1 livre = 453.6 g) et inversement.

### Un formulaire
Dans votre document HTML, créer un formulaire contenant:

1. un champs de texte pour entrer une valeur à convertir
2. un sélecteur pour choisir l’unité cible (centimètres, pouces, grammes ou livres) [https://www.
w3schools.com/jsref/tryit.asp?filename=tryjsref_option_value](https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_option_value)
3. un bouton qui permettra de déclencher une fonction javascript qui réalisera la conversion
4. en dehors de votre formulaire, créez également un paragraphe vide identifié (qui servira à écrire le
résultat de la conversion par votre script)

### Du Javascript pour réaliser la conversion
Dans votre script, mettez en œuvre une fonction qui sera exécutée lorsque le bouton du formulaire
précédent sera cliqué. Votre fonction doit:
1. récupérer la valeur entrée par l’utilisateur dans le champs de texte
2. récupérer l’unité cible spécifiée dans le sélecteur
3. selon l’unité spécifiée, calculer la conversion (par exemple, si l’unité choisie est "pouces" alors on
réalise une conversion centimètres → pouces)
4. affichez le résultat de votre conversion dans le paragraphe que vous avez précédemment identifié
dans votre document HTML
