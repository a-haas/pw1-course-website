---
layout: ../layouts/AirThemeLayout.astro
title: "CSS Is Awesome"
author: "Antoine Haas"
date: "27 septembre 2022"
---

Pour un cours en ligne plus détaillé : [https://web.dev/learn/css](https://web.dev/learn/css)

## Les bases pour écrire du CSS

```css
p {
  width: 100px;
  height: 50px;
  padding: 20px;
  border: 1px solid;
}
```

Décomposons le code ci-dessus :

* `p` (eg, la partie avant l'accolade) permet de cibler l'élément HTML désiré. On appelle cette partie le sélecteur.
* `{ ... }` les accolades permettent de définir le bloc de code qui nous permettra d'y ajouter les règles qui s'appliqueront sur notre sélecteur
* `... : ...;` permet de décrirer une règle CSS 
* `width`, `height`, `padding`, `border` sont les propriétés de chacune de leur règle respective
* `100px`, `50px`, `20px`, `1px solid` sont les valeurs de leur propriété respective    

## Box Model
<img src='/cm-2/box-model.png' alt="box-model image" />

*Crédits : [https://web.dev/learn/css/box-model/](https://web.dev/learn/css/box-model/)*

La taille du rendu d'un élément HTML est définit par 4 éléments (décrit dans la page ci-dessus) :

1. Content : la taille que prendra le contenu HTML. On entent par contenu le texte, les images, les sous-éléments, etc...
2. Padding : l'espace entre la bordure d'un élément et le contenu. Par exemple le padding sert à donner du *"corps"* à un bouton.
3. Border : la taille de la bordure d'un élément.
4. Margin : l'espace entre l'élément HTML actuel et ses parents / voisins

Par défaut dans les navigateurs le modèle de box utilisé est le *content box* pour définir la largeur et la hauteur de l'élément HTML.
Il est plutôt conseillé (bien que non obligatoire) d'utiliser le *border-box* pour se faciliter la vie lors de la création de son site web.
Ce changement est souvent effectuer dans les reset CSS ou les libraires CSS. Sans aller aussi loin vous pouvez également utiliser le code CSS ci-dessous :

```css
*, :after, :before {
    box-sizing: border-box;
}
```

Voici un tableau récapitulant les valeurs possibles pour la propriété box-sizing

| Valeur      | Taille                                  |
| ----------- | --------------------------------------- |
| content-box | Contenu de l'élément uniquement         |
| border-box  | Contenu de l'élément + padding + margin |

## Les sélecteurs
Les sélecteurs sont le moyen de cibler les éléments désirés de notre code HTML.
Une liste variée de ces sélecteurs existe (d'autant plus depuis CSS3) et il est possible de les combiner afin d'obtenir un maximum de flexibilité.

En voici quelques uns :

| Sélecteur     | Cible                                                             | Exemples de sélecteur CSS    | Eléments ciblés dans le HTML                             |
| ------------- | ----------------------------------------------------------------- | ---------------------------- | -------------------------------------------------------- |
| `tag-name`    | Tous les tags ayant le nom *tag-name*                             | p, a, div, html, img, etc... | `<p></p>, <a></a>, <div></div>, <html></html>, <img />`  |
| `.classname`  | Tous les tags ayant l'attribut class avec pour valeur *classname* | .content, .my-list, etc...   | `<div class="content"></div>, <ul class="my-list"></ul>` |
| `#identifier` | Le tag ayant l'identifiant **UNIQUE** identifier                  | #mon-id, #main-content       | `<img id="mon-id" />, <div class="main-content"></div>`  |
| *             | Sélecteur universel                                               | *                            | Tous les éléments HTML                                   |

Ces sélecteurs sont les principaux que vous aurez besoin d'utiliser pour commencer à styliser vos premières pages.
Pour plus de flexibilité vous pouvez les combiner :

| Combinaison                           | Cible                                                                                                      | Exemples de sélecteur CSS | Eléments ciblés dans le HTML             |
| ------------------------------------- | ---------------------------------------------------------------------------------------------------------- | ------------------------- | ---------------------------------------- |
| `selecteur-1, selecteur-2`            | Tous les éléments correspondant soit au selecteur selecteur-1 soit selecteur-2                             | `div, .div`               | `<div></div>, <span class="div"></span>` |
| `selecteur-parent > selecteur-enfant` | Tous les éléments correspondant au sélecteur selecteur-enfant et ayant pour parent direct selecteur-parent | `div > .div`              | `<div><span class="div"></span></div>`   |
| `selecteur-1selecteur-2`              | Tous les éléments correspondant selecteur selecteur-1 **ET** selecteur-2                                   | `div.div`                 | `<div class="div"></div>`                |

## Règles & style sheets en cascade
Les  règles CSS qui seront appliqués à votre site web peuvent provenir de très nombreux endroits. 

**Celles que vous pouvez utiliser dans votre page et sur lesquelles vous avez la main :**
* Balise HTML link `<link rel="stylesheet" href="..."/>`, il s'agit de la manière d'ajouter le CSS qui est recommandée et la plus standard.
* Insertion d'un balise HTML `<style>/** your CSS code goes here */</style>` directement dans votre page HTML. 
  Il ne s'agit pas de la méthode la plus recommandée car vous ne pourrez pas réutiliser votre code CSS au sein d'autres pages. 
* Application d'un attribut style pour appliquer un style à un élément HTML particulier `<button class="..." style="/** your inline CSS code goes here */">...</button>`.

**Celles de l'utilisateur et sur lesquelles vous n'avez pas la main :**
* Le CSS par défaut du navigateur
* Le CSS spécifique de l'utilisateur (préférences du système d'exploitation ou rajout via une extension).

Nous allons voir comment ces ajouts de règlmes fonctionnent et s'imbriquent entre elles et ainsi fournir le rendu final. 

### Par défaut, celui du navigateur
Le navigateur fourni un style par défaut. Il est évidemment impacté par les préférences du système d'exploitation ou précisé par l'utilisateur dans le menu des options.

Ce style par défaut est celui que nous avons utilisé lors du premier CM ainsi que le premier TP.

Cependant son utilisation reste très rare et il n'est utilisé que dans des sites très simples tels que les blogs qui souhaitent conserver les préférences utilisateurs et n'appliquer qu'un style minimaliste.
La majorité des sites web utiliseront un reset du CSS (tel que celui-ci [https://meyerweb.com/eric/tools/css/reset/](https://meyerweb.com/eric/tools/css/reset/)) pour fournir un rendu homogène et similaire sur tous les navigateurs et plateforme.

### Spécificité
Pour pouvoir appliquer l'ensemble des styles et résoudre les conflits qui pourrait se produire, les moteurs de rendu CSS se base sur des règles dites de spécificité.

On ne rentrera pas dans les détails complets des règles de spécificité dans le cadre du cours mais sont très bien décrites ici : [https://web.dev/learn/css/specificity/](https://web.dev/learn/css/specificity/)

Pour faire vos premiers design de style et pour comprendre les bases de l'application des règles CSS nous allons voir les principales règles (du groupe avec la priorité la plus faible au groupe à la priorité la plus importante).

1. Le sélecteur universel : `*`
2. Sélecteur de tag HTML : `div { ... }`
3. Sélecteur de classe : `.my-class { ... }`
4. Sélecteur d'ID : `#my-id { ... }`
5. Le style inline : `<div style="..."></div>`
6. Et enfin la règle CSS !important qui prime sur tout et à utiliser en dernier recours : `div { color: red !important; }`

La combinaison des sélecteurs augmente la priorité de ses règles. Nous pouvons cependant utiliser l'heuristique suivante pour simplifier :
* Le groupe de priorité d'une combinaison d'un sélecteur sera le groupe de son sélecteur avec la priorité la plus haute.
* Au sein d'un même groupe de priorité la priorité est donnée au sélecteur avec le plus de combinaison.
* Si deux règles ont la même priorité de sélecteur, alors la règle qui apparaît en dernier 
  (par exemple une balise `<style></style>` en dessous d'une balise `<link />` aura le dernier si la priorité est la même et que les règles sont contradictoires).  

### Hériter
Pour finir sur l'ordre d'application des règles, il est nécessaire de savoir que certaines règles sont héritées des éléments parents, par exemple :

**Le code CSS**

```css
html {
  color: gray;
}

body {
  font-size: 1.2em;
}

article {
  font-style: italic;
}
```

**Le code HTML**
```html
<html>
  <body>
    <article>
      <p>Lorem ipsum dolor sit amet.</p>
    </article>
  </body>
</html>
```

Le texte *"Lorem ipsum dolor sit amet."* dans la balise HTML `<p>...</p>` héritera des règles de ses tags parents, car ce sont des règles dites héritables. 
Le texte sera ainsi de couleur grise, avec une taille de 1.2em et en italique.

Les règles héritables sont dans le cas général des règles sur l'affichage propre au texte ou au contenu et non sur le positionnement de ceux.

En voici quelques unes : (vous pouvez retrouver l'intégralité ici [https://web.dev/learn/css/inheritance/](https://web.dev/learn/css/inheritance/))

* color
* direction
* font-family
* font-size
* font-style
* font
* text-align
* text-indent
* word-spacing
* list-style

## On pose les bases
Maintenant que les notions inhérentes aux CSS ont été évoquées, nous allons nous intéresser aux règles fondamentales.

Nous évoquerons ici 
Seules les règles les plus utiles de la version 3 seront ainsi évoquées et les règles les plus utiles déjà présentes dans la version 2 de CSS.

### Sizing units
Pour gérer au mieux l'affichage, de nombreuses règles se servent de valeurs avec des unités, appelées unités de dimensionnement ou sizing units, pour jouer sur la taille de la règle appliquée.

On peut les séparer en deux catégories, les unités absolues et relatives.

**Unités absolues :**

| Unité | Description                                                                           | Exemple |
| ----- | ------------------------------------------------------------------------------------- | ------- |
| `px`  | Les pixels, l'unité absolue la plus utilisée                                          | 100px   |
| `cm`  | Les centimètres, très peu utilisée hormis pour le CSS d'impression                    | 100cm   |
| `pt`  | Les points, utilisées pour l'impression et éventuellement pour la taille de la police | 14pt    |

  D'autres unités absolues existent telles que : *mm* (millimeters), *Q* (Quarter-millimeters), *in* (inches), *pc* (picas)

**Unités relatives**

| Unité | Description                                                                       | Exemple     |
| ----- | --------------------------------------------------------------------------------- | ----------- |
| `%`   | Pourcentage de la taille de l'élement courant                                     | 150%        |
| `em`  | Unité relative à la taille de la police de caractère de son parent                | 0.25em, 2em |
| `rem` | Unité relative à la taille de la police de caractère de la page (par défaut 16px) | 3rem        |
| `vw`  | Largeur de la taille de l'écran (1vw = 1% de la largeur de l'écran, 100vw = 100%) | 35vw        |
| `vh`  | Comme *vw* mais par rapport à la hauteur de l'écran                               | 100vh       |

### Layout
Le positionnement des éléments est régit par plusieurs règles CSS qui tombent dans la catégorie du *layout*.

Nous allons voir les principales propriétés, à savoir *display*, *float*, *position*. 

***display***
La propriété *display* permet de définir le comportement de la *box* (cf. la partie box model) de l'élément. Les trois valeurs les plus classiques sont :
* inline : l'élement prend le minimum de place et n'impacte pas les éléments suivants. Un élément inline peut être comparé à un mot dans une phrase.
  Typiquement, l'élément `<span></span>`, `<strong></strong>` ou encore `<a href="..."></a>` sont par défaut (et sémantiquement) inliné.
  Les éléments inlinés n'ont ni padding, ni margin sur leur hauteur. 
* block : l'élement prend le maximum de place que peut occuper son contenu et prend toute la place en largeur (de son élément parent) si la taille n'est pas spécifiée.
  L'élément saute également une ligne. Les éléments `<div></div>`, `<header></header>`, `<section></section>` et `<body></body>` sont des éléments de type block.
* inline-block : un élément inline-block allie les avantages des éléments inline et block. Il reste inliné s'il a la place nécessaire (sinon il saute une ligne)
  mais contrairement aux éléments inliné un élément inline-block peut utilisé les margin et padding sur sa hauteur.
  
Voici une image récapitulative :

<img src='/cm-2/display-properties.png' alt="display properties image" />

*Crédits : [https://web.dev/learn/css/layout/](https://web.dev/learn/css/layout/)*

**float**
La propriété *float* permet de mettre un élément à droite ou à gauche de son bloc. Le texte et les autres éléments du bloc peuvent ainsi 
se positionner autour de celui-ci. Un élément qui utilise la propriété *float* se verra retirer du flow de son bloc (c'est à dire qu'il ne sera pas pris en compte, par exemple lors du calcul de la hauteur de son bloc).
Il reste cependant dans le flow global de la page.

```css
img {
	float: left;
	margin-right: 1em;
}
```

<img src='/cm-2/float.png' alt="float property image" />

*Crédits : [https://web.dev/learn/css/layout/](https://web.dev/learn/css/layout/)*

**position**
La propriété *position* permet de sortir un élément du flow de la page et de le placer précisément en utilisant les propriétés *top*, *left*, *right* et *bottom*.

Voici les valeurs possibles :

* static : le fonctionnement par défaut, on utilise pas la position
* absolute : position définie par rapport à la page (`top: 0; left: 0;` positionnera l'élément en haut à gauche de la page)
* relative : positive définie par rapport au bloc de l'élément (`top: 0; left: 0;` positionnera l'élément en haut à gauche de son bloc)
* fixed : position définie par rapport à l'écran (`top: 0; left: 0;` positionnera l'élément en haut à gauche de l'écran, l'élément sera toujours en haut à gauche, même si l'utilisateur scroll)
* sticky : position définie par rapport à la page comme la position *absolute* mais une fois atteinte devient une position *fixed*.

La section try it de la page [https://developer.mozilla.org/en-US/docs/Web/CSS/position](https://developer.mozilla.org/en-US/docs/Web/CSS/position) permet d'essayer très facilement chacunes des positions.

Des layouts plus modernes et avancés seront évoqués dans les prochains cours.

### Spacing (padding, margin)
La notion d'espacement des éléments HTML a pu être entreaperçue dans les chapitres précédents.

**Margin** : La margin est l'espace requis entre un bloc et le suivant. Par exemple le code CSS ci-dessous indique de laisser un espacement en haut de l'élément de 3em pour chacune des divs, et 1.5em en bas.
```css
div {
  margin-top: 3em;
  margin-bottom: 1.5em;
}
```

Voici un dessin schématique en utilisant 3 divs et le code ci-dessus.

<img src='/cm-2/margin-examples.png' alt="margin example image" />

La propriété *margin* peut prendre la valeur *auto*, dans ce cas-ci elle occupe la place disponible laissée par son élément et les autres autour. 
Elle est utile pour centrer un élément de *display: bloc* avec une taille inférieure à son bloc parent.

**Padding** : Le padding est l'espace intérieur requis par un élément HTML. Prenons exemple sur un cas typique d'utilisation du padding pour ajouter du corps à un bouton.

```css
.btn {
  padding-top: 1em;
  padding-bottom: 1.5em;
  padding-left: 2em;
  padding-right: 2em;
}
```

<img src='/cm-2/padding-examples.png' alt="padding example image" />

### Bordures
Les bordures permettent de délimiter un élément HTML.
On peut influer sur son style (*border-style*), sa taille (*border-width*), sa couleur (*border-color*) ou encore sa forme (*border-radius*) en utilisant les bonnes propriétés CSS.

Il est également possible de n'impacter qu'un des côtés de la bordure en utilisant *border-top-\**, *border-right-\**, *border-bottom-\**, *border-right-\**.
Par exemple, *border-top-style*.

### Overflow
L'overflow se produit lorsqu'un élément est trop grand ou large par rapport à son contenant. Dans ce cas précis il est possible d'utiliser la propriété CSS *overflow* pour définir la stratégie d'overflow.

| Stratégie | Description                                                                       |
| --------- | --------------------------------------------------------------------------------- |
| `visible` | Le contenu en overflow est visible et déborde sur les éléments voisins            |
| `hidden`  | Tout contenu en overflow dans l'élément est caché et n'apparait pas               |
| `scroll`  | Les barres de scrolls apparaissent **même s'il n'y a pas de contenu en overflow** |
| `auto`    | Les barres de scrolls apparaissent **uniquement s'il y a du contenu en overflow** |

Il est aussi possible de choisir une stratégie uniquement pour un overflow en hauteur ou en largeur en utilisant les propriétés *overflow-x* et *overflow-y*. 

La section try it de la page [https://developer.mozilla.org/en-US/docs/Web/CSS/overflow](https://developer.mozilla.org/en-US/docs/Web/CSS/overflow) permet d'essayer très facilement chacune des valeurs.

**text-overflow**
Une autre possibilité est de gérer le surplus au niveau du texte en utilisant la propriété *text-overflow*.

| Stratégie   | Description                                                                       |
| ----------- | --------------------------------------------------------------------------------- |
| `clip`      | Le texte en surplus n'apparait pas                                                |
| `ellipsis`  | Une elipse (...) remplace le texte en surplus                                     |
| `"<value>"` | Remplace le texte en surplus par la chaîne *\<value\>*                              |

### Background
Pour égayer le design de la page il est possible de définir le fond d'un élement. Il peut s'agir soit d'une image, soit d'une couleur, soit d'un dégradé.

La propriété *background* sert à faire celà.
Une documentation exhaustive de chacune des sous-propriétés est disponible à cette adresse [https://developer.mozilla.org/en-US/docs/Web/CSS/background](https://developer.mozilla.org/en-US/docs/Web/CSS/background). 

### Un petit pop de couleur
La couleur en CSS est utiliser pour de nombreuses propriétés telles que *color*, *background-color*, *border-color*, etc...

Ces propriétés acceptent des valeurs de type couleur qui peuvent être sous plusieurs formes, en fonction des besoins :
* Le type hexadécimale (de #000000 à #FFFFFF) qui représente la couleur RGB sur 3 chiffres hexadécimaux de 0 (00) à 255 (FF). Le premier étant pour le rouge, le deuxième pour le vert et le dernier pour le bleu.
* Les couleurs nommées telles que *white*, *transparent*, *black*, *blue*, *red*, *tomato*, etc...
* La fonction *rgb(r, g, b)* qui prend trois chiffres de 0 à 255 pour représenter le rouge, le vert et le bleu
* La fonction *rgba(r, g, b, alpha)* qui est similaire à la fonction *rgb()* mais qui permet de rajouter de la transparence avec l'argument *alpha* qui est un pourcentage (entre 0 et 1)
* La fonction *hsl(h,s%,l%)* pour définir la couleur à partir de la teinte, de la saturation et de la luminosité. Par exemple *hsl(9,100%,64%)*.
* La fonctin *hsla(h,s%,l%, alpha)* qui est similaire à la fonction *hsl()* mais qui permet de rajouter de la transparence avec l'argument *alpha*.

### Typographie

#### Texte
Pour styliser le texte de son site web il est possible d'utiliser les propriétés CSS *font-\** :
* *font-weight* pour mettre en gras ou en light le texte
* *font-size* pour choisir la taille du texte
* *font-style* pour mettre le texte en italique

Pour habiller son texte un grand nombre de propriétés existent :
* *line-height* pour choisir l'espacement des lignes du texte
* *letter-spacing* pour choisir l'espacement entre les lettres
* *word-spacing* pour choisir l'espacement entre les mots
* *text-decoration* pour souligner ou barrer le texte
* *color* pour choisr la couleur du texte
* *text-indent* pour indenter ses paragraphes
* *word-break* pour indiquer la stratégie de retour à la ligne du texte
* *text-align* pour aligner le texte (droite, gauche, centré, justifié)
* *direction* et writing-mode pour choisir la direction du texte (gauche vers la droite; droite vers la gauche; haut vers le bas; bas vers le haut) en fonction de la langue

Un article plus exhaustif est disponible à cette adresse [https://web.dev/learn/css/typography/](https://web.dev/learn/css/typography/)

#### Police
La police de caractères est définie via la propriété *font-family*.

Il est possible d'utiliser les polices du système d'exploitation ainsi qu'une police téléchargée par la page, via Google Fonts ([https://fonts.google.com/](https://fonts.google.com/)) par exemple.

```css
p {
  font-family: Segoe UI,system-ui,-apple-system,sans-serif;
}
```

Il est possible d'en définir plusieurs, la police choisie étant la première qui existe dans l'ordre de lecture.

On peut également se servir des familles de police par défaut du système d'exploitation, permettant ainsi de respecter au mieux les préférences de l'utilisateur.
* *serif* (par exemple Times New Roman, Georgia, Garamond)
* *sans-serif* (par exemple Helvetica, Arial, Roboto)
* *monospace* (par exemple Consoles, Hack, Source Code Pro) # side-note, les fonts monospace sont les plus utilisées dans un IDE pour coder
* *cursive* (par exemple Comic Sans)