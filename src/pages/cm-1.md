---
layout: ../layouts/AirThemeLayout.astro
title: "Le web et ses fondamentaux"
author: "Antoine Haas"
date: "27 août 2022"
---

## Les premiers pas d'Internet
Internet vu le jour en 1990 suite à la dématérialisation d'ARPANET (l'ancêtre militaire d'Internet).
Divers protocoles d'échanges de données (tel que TCP/IP) ont pu voir le jour durant la période ARPANET, et ce n'était qu'une question de temps (en 1991) avant que le ***World Wide Web (WWW)*** soit créé par le fameux Tim Berners Lee, facilitant ainsi l'échange de textes interconnectés (hypertexte) et de médias.
Le protocole d'échange des données **HTTP** et le langage de rendu **HTML** étaient nés.

D'autres outils seront créés et évolueront au fil des années :
* les navigateurs internet (Netscape, Internet Explorer, Opera, Firefox, Chrome, Safari, Brave, etc...)
* les moteurs de recherches (Yahoo, Google, Duckduckgo, Qwant, etc...)
* le langage CSS, pour modifier l'apparence de son site web
* le langage Javascript, pour dynamiser les pages web

Voici quelques dates clés :
* 1989 - 1991
  * création et *"mise à disposition"* du World Wide Web sur un serveur interne du CERN
  * création du protocole HTTP
  * création du langage HTML
  * création du premier navigateur
* 1994 
  * création du langage CSS 
  * création du navigateur Netscape
  * création du moteur de recherche Yahoo
* 1995
  * création du langage Javascript
  * création d'Internet Explorer
* 1997
  * première publication des spécifications d'HTML 4.0
  * première publication des spécifications de CSS 2
* 1998 : création du moteur de recherche Google
* 1999 : début des travaux sur les nouvelles spécifications modulaires de CSS 3
* 2002 : création du navigateur Firefox
* 2008 : création du navigateur Google Chrome
* 2010 - 2020 : Après plus de 10 ans d'échec et de désillusion, plusieurs modules de CSS 3 commence enfin à être adopté plus massivement
* 2014 : lancement d'HTML 5
* 2015 : publication d'une version remodelée de Javascript nommée Ecmascript2015 ou bien encore ES6
* **17 juin 2022** : Mort officielle d'IE 11

Voici quelques liens qui abordent plus en détails certains des points ci-dessus :
* [https://www.01net.com/actualites/les-15-dates-qui-ont-fait-le-web-615826.html](https://www.01net.com/actualites/les-15-dates-qui-ont-fait-le-web-615826.html)
* [https://home.cern/fr/science/computing/birth-web/short-history-web](https://home.cern/fr/science/computing/birth-web/short-history-web)
* [https://fr.wikipedia.org/wiki/World_Wide_Web](https://fr.wikipedia.org/wiki/World_Wide_Web)
* [https://www.mozilla.org/fr/firefox/browsers/browser-history](https://www.mozilla.org/fr/firefox/browsers/browser-history)

La suite du cours se concentrera sur la réalisation des sites web les plus simples (des années 90) aux plus complexes (ceux de ses dernières années).

## HTML qu'est ce que c'est ?

**En quelques mots :** HTML (*HyperText Markup Language*) est la base d'un site web; il s'agit de sa structure ainsi que de son contenu.

Par essence HTML est un langage de markup (tel que *LaTeX*, *Markdown*, *XML* et bien d'autres) qui se caractérise 
par l'utilisation d'un ensemble de tags permettant la structuration et la hiérarchisation du contenu. La majorité des tags HTML peuvent être classés dans une des deux catégories ci-dessous.

Vous trouverez également la liste exhaustive et détaillée des tags à cette adresse : [https://fr.w3docs.com/apprendre-html/tableau-des-tags-html.html](https://fr.w3docs.com/apprendre-html/tableau-des-tags-html.html) 

### Les tags de *structuration*
Les tags de *structuration* permettent et, n'ont d'ailleurs pour unique objectif, de structurer et hiérarchiser le code HTML sans affecter l'affichage ou le rendu de la page. 
Voici les principaux et les plus utiles :

| Tag          | Description                                                                            |
| ------------ | -------------------------------------------------------------------------------------- |
| `<!DOCTYPE>` | Définit le type du document.                                                           |
| `<html>`     | Définit le début du HTML                                                               |
| `<head>`     | Balise qui permet de définir les métadatas et autres balises de description de la page |
| `<body>`     | Balise qui contient le coeur du contenu HTML                                           |
| `<meta>`     | Une balise de métadata                                                                 |
| `<!-- -->`   | Insérer un commentaire dans le code HTML                                               |

### Les tags de *contenu*
Les tags de *contenu* ou de *stylisation* permettent d'afficher le contenu sur une page. Chaque navigateur possède son propre style par défaut.
Voici les principaux et les plus utiles :

| Tag                         | Description                                        |
| --------------------------- | -------------------------------------------------- |
| `<h1>`, `<h2>`, ..., `<h6>` | Les titres de la page                              |
| `<p>`                       | Un paragraphe                                      |
| `<br>`                      | Un saut de ligne                                   |
| `<hr>`                      | Un espace horizontal pour diviser deux blocs       |
| `<div>`                     | Un bloc de la page                                 |
| `<span>`                    | Un sous-bloc de la page, par exemple dans du texte |
| `<table>`                   | Un tableau                                         |
| `<strong>`, `<b>`           | Mettre en gras du contenu                          |
| `<u>`                       | Souligner du contenu                               |
| `<i>`, `em`                 | Mettre du contenu en italique                      |
| `<img>`                     | Insérer une image                                  |
| `<a>`                       | Un lien hypertexte                                 |
| `<ul>`,  `<ol>`             | Une liste, respectivement non ordonnée et ordonnée |

### Les attributs des tags HTML
Les attributs permettent de personnaliser le fonctionnement de chacun des tags HTML 
en précisant élément par élément les comportements spécifiques de chacun des tags.

Voici les principaux et les plus utiles :

| Attribut | Description                                                                                                                    | Tags        |
| -------- | ------------------------------------------------------------------------------------------------------------------------------ | ----------- |
| id       | Attribut permettant de définir l'identifiant d'un élément. Utile pour le CSS et le JS que nous verrons plus tard dans le cours | Universel   |
| class    | Attribut permettant de définir une classe d'élément. Utile pour le CSS et le JS que nous verrons plus tard dans le cours       | Universel   |
| title    | Attribut permettant de décrire une balise. Utile pour l'accessibilité et la clarté des infos                                   | Universel   |
| src      | Le lien source d'une image                                                                                                     | `img`       |
| alt      | Un texte alternatif à afficher lorsque l'élément ne peut pas être affiché                                                      | `img`       |
| href     | Attribut permettant de faire référence à une autre page HTML / lien Internet                                                   | `a`, `link` |
| data-*   | Préfixe d'attributs personnalisés (nous verrons comment les utilisés dans la suite du cours)                                   | Universel   |
| aria-*   | Préfixe d'attributs d'accessibilité                                                                                            | Universel   |

Les attributs de style sont pour la plupart dépréciés et il est conseillé d'utiliser le CSS. Voici une liste de ce type d'attributs :
* border
* bgcolor
* background
* color
* etc...

Vous trouverez la liste exhaustive des attributs et de leur utilité dans la page ci-jointe : [https://developer.mozilla.org/fr/docs/Web/HTML/Attributes](https://developer.mozilla.org/fr/docs/Web/HTML/Attributes)

### Description d'un lien http
Vous trouverez dans le lien ci-joint la description complète d'une page HTTP : [https://developer.mozilla.org/fr/docs/Learn/Common_questions/What_is_a_URL](https://developer.mozilla.org/fr/docs/Learn/Common_questions/What_is_a_URL)

Voici les points qui nous intéressent pour le moment sur l'exemple suivant : `http://www.exemple.com:80/chemin/vers/monfichier.html#anchorId`

1. `http://` est le protocole de communication utilisé. Le plus courant de nos jours est son alternative sécurisée, le protocole `https`
2. `www.exemple.com` est le nom de domaine de notre site 
3. :80, le port du serveur de domaine. Deux ports par défaut existent, 80 pour http et 443 pour https
4. `/chemin/vers/monfichier.html` est le chemin qui permet d'accéder au fichier désiré (ici, monfichier.html)
5. `#anchorId` (partie optionnelle) est une ancre. Une ancre permet de cibler un élément (par son attribut `id`) et de se rendre directement sur celui-ci au chargement de la page. Par défaut (sans ancre), on atterit au sommet de la page.

## Martine et son premier site web

```html
<!-- http://www.brucelawson.co.uk/2010/a-minimal-html5-document/ -->

<!doctype html>
<html lang=en>
  <head>
    <meta charset=utf-8>
    <title>Lorem ipsum</title>
  </head>
  <body>
    <h1>Some lorem ipsum</h1>
    
    <div>
      <h2>Lorem ipsum 1 to 3</h2>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Aliquam sem fringilla ut morbi tincidunt augue interdum. Turpis massa sed elementum tempus egestas sed. Ut faucibus pulvinar elementum integer enim neque volutpat ac tincidunt. Adipiscing at in tellus integer feugiat scelerisque. A condimentum vitae sapien pellentesque habitant morbi tristique senectus et. Bibendum est ultricies integer quis auctor elit sed vulputate. Nisl rhoncus mattis rhoncus urna neque viverra justo nec ultrices. Mauris a diam maecenas sed enim. Consequat nisl vel pretium lectus quam id leo in vitae. At tempor commodo ullamcorper a. Phasellus vestibulum lorem sed risus ultricies tristique nulla. Fusce id velit ut tortor pretium. Posuere urna nec tincidunt praesent semper feugiat nibh. Tellus cras adipiscing enim eu turpis egestas pretium aenean. Tellus molestie nunc non blandit massa.
      </p>

      <p>
  Sed adipiscing diam donec adipiscing. In nibh mauris cursus mattis molestie a iaculis. Mauris commodo quis imperdiet massa tincidunt nunc. Quis eleifend quam adipiscing vitae proin. Etiam erat velit scelerisque in. Scelerisque eu ultrices vitae auctor eu augue ut lectus arcu. Vulputate sapien nec sagittis aliquam malesuada bibendum. Id leo in vitae turpis massa. Vitae suscipit tellus mauris a diam maecenas. Molestie a iaculis at erat pellentesque. Cras sed felis eget velit aliquet sagittis id consectetur purus. Pretium nibh ipsum consequat nisl vel pretium lectus quam. Posuere urna nec tincidunt praesent. In ornare quam viverra orci sagittis eu volutpat odio. Cursus in hac habitasse platea dictumst quisque sagittis.
      </p>

      <p>
  Morbi tincidunt ornare massa eget egestas purus. Feugiat vivamus at augue eget arcu dictum varius duis at. Nisi porta lorem mollis aliquam. Cum sociis natoque penatibus et. Semper risus in hendrerit gravida rutrum quisque non tellus orci. Pellentesque eu tincidunt tortor aliquam. Vestibulum sed arcu non odio. Tempus imperdiet nulla malesuada pellentesque elit eget gravida. Fringilla phasellus faucibus scelerisque eleifend. Tortor consequat id porta nibh venenatis cras sed. Iaculis urna id volutpat lacus laoreet. Pretium vulputate sapien nec sagittis aliquam malesuada bibendum. Nulla porttitor massa id neque aliquam vestibulum. Erat velit scelerisque in dictum. Sed viverra tellus in hac habitasse platea dictumst vestibulum. Egestas congue quisque egestas diam in arcu. Neque convallis a cras semper auctor neque. Dis parturient montes nascetur ridiculus mus mauris vitae.
      </p>
    </div>
    
    <div>
      <h2>Lorem ipsum 4 to 5</h2>
        <p>
    Tortor at risus viverra adipiscing. Morbi blandit cursus risus at ultrices. Eleifend donec pretium vulputate sapien nec sagittis aliquam. Metus aliquam eleifend mi in nulla posuere. Lorem donec massa sapien faucibus et. Netus et malesuada fames ac turpis egestas sed tempus urna. Eu volutpat odio facilisis mauris sit amet massa vitae. Purus gravida quis blandit turpis cursus in hac habitasse. Gravida arcu ac tortor dignissim convallis aenean et tortor at. Diam maecenas ultricies mi eget mauris pharetra et. Id leo in vitae turpis massa sed elementum tempus. Risus quis varius quam quisque. Varius quam quisque id diam vel. Id consectetur purus ut faucibus pulvinar elementum integer enim. Pretium lectus quam id leo in vitae turpis massa sed. Et netus et malesuada fames ac turpis. Iaculis eu non diam phasellus vestibulum lorem sed. Nibh mauris cursus mattis molestie.
        </p>
        <p>
    In aliquam sem fringilla ut morbi tincidunt augue interdum. Sodales ut etiam sit amet nisl purus in mollis nunc. Dignissim diam quis enim lobortis scelerisque fermentum dui. Auctor urna nunc id cursus metus aliquam. Fusce ut placerat orci nulla pellentesque dignissim. Congue eu consequat ac felis. Vel quam elementum pulvinar etiam non quam. Lectus urna duis convallis convallis tellus. Aliquet eget sit amet tellus cras adipiscing enim. Volutpat commodo sed egestas egestas fringilla. Ullamcorper velit sed ullamcorper morbi tincidunt. Consectetur a erat nam at lectus urna duis convallis convallis. Aliquam vestibulum morbi blandit cursus risus at. Cursus euismod quis viverra nibh cras pulvinar mattis nunc. Proin fermentum leo vel orci. Rutrum quisque non tellus orci ac auctor augue mauris. Malesuada proin libero nunc consequat interdum varius sit. Et malesuada fames ac turpis egestas integer eget aliquet.
        </p>
    </div>
  </body>
</html>
```