---
layout: ../layouts/AirThemeLayout.astro
title: "TP 6 - ES6, Ajax et JS avancé"
author: "Antoine Haas"
date: "30 novembre 2022"
---
*Université de Strasbourg - UFR Mathématique & Informatique - L1S2 - Programmation Web 1*

**Commencer à utiliser les fonctionnalités les plus récentes et plus avancées de Javascript**

Vous privilégierez un maximum les fonctionnalités offertes par ES6 tout au long du TP.

## Partie 1

L'objectif de ce TP est de récupérer les produits les plus vendus (par nombre de chalets, et dans **l'ordre décroissant**) du marché de Noël strasbourgeois de 2019.

Pour ce faire nous allons utiliser l'API mise à disposition par *Open Data Strabourg.eu*. De nombreux jeux de données existent pour les différents domaines et enjeux de la CUS mais celui qui nous intéresse est disponible à l'adresse suivante : [https://data.strasbourg.eu/explore/dataset/noel-2019-produit-unique/api/?rows=10](https://data.strasbourg.eu/explore/dataset/noel-2019-produit-unique/api/?rows=10)

Vous pouvez visualiser le retour de l'API (au format *JSON* sur la droite de la page).

Une fois que vous avez pu analyser et comprendre le résultat de l'API, récupérer celui-ci en appelant (à l'aide de la fonctionnalité AJAX de Javascript) via l'URL [https://data.strasbourg.eu//api/records/1.0/search/?dataset=noel-2019-produit-unique&q=&rows=253](https://data.strasbourg.eu//api/records/1.0/search/?dataset=noel-2019-produit-unique&q=&rows=253).

Une fois récupéré, afficher le tout dans la console de votre navigateur.

Vous pourrez vous servir du bout de code suivant :

```js
function ajax(url, f){
  var xmlhttp;
  // compatible with IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function(){
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
          f(xmlhttp.responseText);
      }
  }
  xmlhttp.open("GET", url, true);
  xmlhttp.send();
}
```

et de la documentation suivante : [https://developer.mozilla.org/fr/docs/Web/API/XMLHttpRequest](https://developer.mozilla.org/fr/docs/Web/API/XMLHttpRequest)

Ou de celui-ci pour une version plus moderne :

```js
fetch('http://example.com/movies.json')
  .then((response) => response.json())
  .then((data) => console.log(data));
```
et de la documentation suivante :
[https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)

## Partie 2

Après avoir récupéré le contenu de l'API vous allez devoir définir une fonction Javascript qui vous permettra de l'exploiter. Pour cela : 

1. Parsez (ou désérialisez) le JSON récupéré afin de créer un objet Javascript utilisable pour la suite.
2. Récupérez uniquement les informations qui nous intéressent dans l'objet nouvellement créé (à savoir un tableau contenant la description des produits et le nombre de chalet vendant ce produit). Vous pourrez vous servir de la déstructuration ou de la fonction *map* pour réaliser cela.
3. Triez enfin le tout pour obtenir les produits les plus vendus par nombre de chalets, dans **l'ordre décroissant**.

## Partie 3

Pour la partie 3 nous allons nous charger de réaliser l'affichage suivant :

<img src="/tp-6/header.png" />
<img src="/tp-6/main.png" />

Pour ce faire vous devrez :
1. Ajouter la librairie MustardUI via le CDN `<link rel="stylesheet" href="https://unpkg.com/mustard-ui@latest/dist/css/mustard-ui.min.css" />`
2. Ajouter la librairie ChartJS via le CDN `<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>`

Voici le lien vers leur documentation respective :
* [https://kylelogue.github.io/mustard-ui/docs/installation/index.html](https://kylelogue.github.io/mustard-ui/docs/installation/index.html)
* [https://www.chartjs.org/docs/latest/](https://www.chartjs.org/docs/latest/)

Pour l'implémentation du style du site en utilisant MustardUI vous aurez besoin notamment des éléments suivants :
* Components > Header
* Components > Card
* Utility Classes > Flexbox Grid

Pour l'implémentation du graphique avec *Chart.js* vous aurez besoin de la documentation suivante notamment :
[https://www.chartjs.org/docs/latest/charts/doughnut.html](https://www.chartjs.org/docs/latest/charts/doughnut.html)

Les données à fournir en entrée du graphique de type *donut* doivent sont en fait un classement des différents types d'articles permettant de visualiser la proportion de présence des articles en les regroupant ensemble.

* La proportion occupé par les 25 articles les plus vus
* La proportion occupé par les 25 articles les plus vus en deuxième
* etc...

Pour grouper vos données vous aurez besoin de la fonction suivante :

```js
const splitToChunks = (array, parts) => {
  let result = [];
  for (let i = parts; i > 0; i--) {
      result.push(array.splice(0, Math.ceil(array.length / i)));
  }
  return result;
}

// example : to split array in 10 groups
chunks = splitToChunks(data, 10)

// example : to split array in 5 groups
chunks = splitToChunks(data, 5)
```

## Partie 4
Déployer votre site fraîchement conçu sur le Gitlab de l'Université en vous servant de la documentation disponible sur Moodle.

## Partie 5
En utilisant les classes disponibles dans MustardUI, rendez votre site responsive. 