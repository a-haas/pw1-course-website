---
layout: ../layouts/AirThemeLayout.astro
title: "TP 2 - Starting with CSS"
author: "Antoine Haas"
date: "27 septembre 2022"
---

## Avant toute chose
La réalisation de ce TP suppose que vous avez bien structuré votre document web, soit en utilisant
des balises génériques de type bloc (`<div>`) soit en utilisant les éléments sémantiques apportés par
HTML5.

**La bonne compréhension du TP n°1 est donc un pré-requis !**

**Objectif du TP :** 
agencer une page web d’une part en modifiant les propriétés d’affichage des blocs
Cette structuration vous permet de disposer d’éléments clairement identifiées dans votre page (en-tête, corps de page,
pied de page et possiblement des blocs disposés autour du corps de texte – un menu de navigation par exemple, ou un
bloc affichant des nouvelles)

On propose de décomposer la page en 5 éléments principaux : l’en-tête, contenant le titre de la
page ; un bloc disposé à gauche pour le menu de navigation ; le corps de la page ; un bloc à droite
dont on peut imaginer qu’il contiendrait des commentaires postés par des visiteurs ; un pied de page.

<img src="/tp-2/layout.png">

## Travail préliminaire
1. Définissez les classes / identifiants des différents blocs dans votre feuille de style.
2. Définissez des marges internes / externes pour aérer la disposition des blocs et leur contenu.
Quelles propriétés CSS utilisez-vous pour cela ?
3. Définissez les largeurs de ces blocs (en particulier les trois blocs juxtaposés). Utilisez vous
des tailles absolues ou relatives ? Pourquoi ?

## Agencement en modifiant le comportement des blocs
Une façon simple et peu contraignante d’agencer les éléments d’une page web est de modifier le
comportement des blocs (leur façon de s’agencer les uns par rapport aux autres).
1. Quel est le comportement par défaut des éléments inline ? et pour les blocs ?
2. Quelle propriété CSS pouvez-vous utiliser pour modifier le comportement de vos blocs de
façon à obtenir un agencement tel que présenté sur la figure ci-dessus ?
3. Utiliser cette propriété pour mettre en page votre document HTML avec l’agencement de votre
choix.

## Sélecteurs
Dans le corps de page, on veut ajouter un tableau avec les informations suivantes

| Nom | Nationalité | Date | Temps |
| ---- | ----- | ---- | ---- |
| Usain Bolt | Jamaïcain | 16/08/2009 | 9s58 |
| Asafa Powell | Jamaïcain | 09/09/2007 | 9s74 |
| Justin Gatlin | Américain | 12/05/2006 | 9s77 |
| Maurice Greene | Américain | 16/06/1999 | 9s79 |
| Donovan Bailey | Canadien | 27/07/1996 | 9s84 |

Pour plus de lisibilité, nous voulons alterner les couleurs de fond des lignes, faire ressortir le temps en
gras (toute la colonne) et au survol d’une ligne passer en affichage “dark mode” sur cette ligne
(ecriture en blanc et fond en noir

**Travail à réaliser**

1. Quels types de sélecteurs pouvez vous utiliser pour répondre aux contraintes. Gardez à l’idée
qu’on veut pouvoir changer des données (inverser 2 lignes, déplacer une colonne) et avoir le
moins de modification à apporter.
2. Définir la table avec l’entête et le contenu.
3. Définissez dans la feuille de style les styles nécessaires.
4. Bonus : vous pouvez regardez sur MDN les sélections par colonne dans un tableau et
transformer votre feuille de style si besoin
(https://developer.mozilla.org/fr/docs/Web/HTML/Element/col)

## Bonus : Inception
Reproduisez la page de ce TP en réalisant votre propre code HTML et CSS.

Idéalement utilisez le reset CSS disponible à cette adresse [https://meyerweb.com/eric/tools/css/reset/](https://meyerweb.com/eric/tools/css/reset/).

En cas de difficulté vous pouvez vous servir de l'inspecteur d'élément de votre navigateur.
Cependant ne copiez pas bêtement le code de celui-ci, il faut avant tout le comprendre.