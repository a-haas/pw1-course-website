---
layout: ../layouts/AirThemeLayout.astro
title: "TP 5 - Concevoir une application web"
author: "Antoine Haas"
date: "15 novembre 2022"
---

<p style="text-align: center">
    <strong>
    Concevoir une application minimaliste et apprendre à manipuler les dates en JS
    </strong>
</p>

## Partie 1

Créer un compte à rebours qui permettra de connaître le temps restant avant la prochaine nuit de l'info.

1. Créer un objet *Date* en JS qui aura pour date la prochaine nuit de l'info (le 1er décembre 2022 à 16h35 **cette année**, de manière générale : le premier jeudi de décembre au coucher du soleil). [https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Date](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Date).
2. Créer une fonction qui affichera le compte à rebours sur la page au format : xxx jours xx heures xx minutes xx secondes.
3. Mettre à jour votre objet *Date* **toutes les secondes** et rafraîchir l'affichage de la page en conséquence. Vous pourrez vous aider de la fonction *setTimeout* [https://developer.mozilla.org/fr/docs/Web/API/setTimeout](https://developer.mozilla.org/fr/docs/Web/API/setTimeout).

## Partie 2

Concevez un design CSS pour votre page de compte à rebours.

N'hésitez pas à rechercher directement sur internet des idées de design ou alors en vous inspirant du site de la nuit de l'info [https://www.nuitdelinfo.com/](https://www.nuitdelinfo.com/).

## Partie 3

Modifiez votre page web pour pouvoir y rajouter un formulaire qui vous permettra de faire un compte à rebours pour une date définie par l'utilisateur et des dates précises (Nouvel an, Noël, St-Valentin, Fête nationale, etc...)

Vous pourrez vous servir de l'élément *\<input type="datetime-local" /\>* pour récupérer la date de l'utilisateur et *\<button type="button"\>\</button\>* pour des dates précises.

**N'oubliez de vous servir d'un maximum de fonctionnalités offertes par Javascript, en utilisant l'orienté objet (OO) par exemple.**