---
layout: ../layouts/AirThemeLayout.astro
title: "TP 3 - Approfondir le CSS"
author: "Antoine Haas"
date: "18 octobre 2022"
---

## Introduction
Reprendre l'exercice n°1 du TP2 et le refaire en utilisant les flexbox. Vous pouvez vous reservir de la même HTML si vous le souhaiter.

Pour rappel, le layout doit être le suivant :

<img src="/tp-2/layout.png">

Une fois l'introduction finie, pensez à récupérer les ressources du TP n°3 à cette adresse [https://moodle.unistra.fr/mod/resource/view.php?id=703364](https://moodle.unistra.fr/mod/resource/view.php?id=703364).

## Exercice 1
**Comprendre les *media-queries***

1. Récupérer le dossier d'exercices disponible sur Moodle et ouvrir le fichier *exercise-1.html* et *style.css*.
2. Analyser la structure HTML de la page.
3. Mettre en valeur les éléments HTML en fonction de leur description à l'aide des *media-queries* CSS. Le code CSS qui peut être utilisé pour mettre en valeur est indiqué directement sur la page (dans le bloc `<code>`).
4. Tester votre implémentation à l'aide des *Dev tools* de votre navigateur.

## Exercice 2
**On commence le JS**

1. Récupérer le dossier d'exercice disponible sur Moodle et ouvrir le fichier *exercise-2.html*.
2. Afficher une pop-up de type *alert* lorsque l'utilisateur clique sur le bouton *"Alert on click!"*.
3. Mettre en valeur la *div* de classe *box* lors du clic sur le bouton *"Highlight on click!"*. La classe *highlight* pourra vous être utile. 
4. Écrire un message dans la console du navigateur lors du survol du bouton *Log on hover!*. La console se trouve dans les *Dev tools* du navigateur.

## Pour aller plus loin

### Exercice 3
**Apprendre à utiliser une lib CSS, ici Milligram CSS**

La librairie CSS *Milligram* est disponible à cette adresse : [https://milligram.io/](https://milligram.io/) ainsi que sa documentation disponible sur la même page, en scrollant.

Pour apprendre à utiliser *Milligram*, vous allez implémenter un blog static en seulement quelques lignes de code. Voici les étapes à suivre :

1. Créer deux pages HTML (*blog.html* et *page.html*)

2. Pour utiliser *Milligram* sur vos pages nouvellement créés il vous suffit d'utiliser le bout de code ci-dessous dans chacune d'entre elles (dans la partie `<head>`):

   ```html
   <!-- Google Fonts -->
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
   
   <!-- CSS Reset -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.css">
   
   <!-- Milligram CSS -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/milligram/1.4.1/milligram.css">
   
   <!-- You should properly set the path from the main file. -->
   ```

3. Implémenter votre propre version du blog en vous inspirant des images ci-dessous et en vous aidant de la documentation de *Milligram* :

<img src="/tp-3/miligram-blog-1.png" alt="Première image du blog" />
<img src="/tp-3/miligram-blog-2.png" alt="Deuxième image du blog" />
<img src="/tp-3/miligram-blog-3.png" alt="Troisième et dernière image du blog" />

### Exercice 4
**Approfondir sa compréhension du CSS et revoir les acquis des précédents TP**

1. Réaliser les exercices CSS disponibles à l'adresse suivante [http://flukeout.github.io](http://flukeout.github.io/) pour améliorer votre utilisation des sélecteurs CSS.
2. Réaliser les exercices CSS disponibles à l'adresse suivante [https://flexboxfroggy.com](https://flexboxfroggy.com/) pour améliorer votre compréhension des flexbox.
3. D'autres exemples de jeux pour apprendre le CSS : [https://dev.to/devmount/8-games-to-learn-css-the-fun-way-4e0f](https://dev.to/devmount/8-games-to-learn-css-the-fun-way-4e0f)