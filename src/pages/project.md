---
layout: ../layouts/AirThemeLayout.astro
title: "Présentation du projet"
author: "Antoine Haas"
date: "28 septembre 2022"
---

## Modalités de rendu
Le projet est à rendre au plus tard le 21 décembre 2021 à 23h59 via la plateforme
Moodle. Il est à réaliser soit seul soit en binôme, durant le temps libre.

Il vous est demandé de rendre un rapport de 6 pages maximum, où vous réaliserez une
petite documentation de votre projet, expliquant comment l’utiliser. Vous justifierez
aussi vos choix d’implémentation, les difficultés rencontrées et les solutions que vous
y avez trouvé. 

Il vous est demandé de présenter des tests de votre application, sous
forme de test case (https://en.wikipedia.org/wiki/Test_case) ou au minimum
expliquez comment vous avez testé votre projet. Enfin, vous présenterez la répartition
des taches dans le groupe.

Vous déposerez sur Moodle, dans une archive au format *.tar.gz* ou *.zip* :
— le rapport au format *.pdf*
— le code du projet, commenté quand c’est nécessaire. Il faut que tous les docu-
ments utilisés par votre page web soient rendus pour qu’elle puisse être testée.

Essayez d’attacher de l’importance à la lisibilité du code (indentation, saut de ligne,
etc.). En JavaScript, les noms de variables et de fonctions doivent avoir des noms
cohérents avec leur rôle.

## Barème indicatif
* HTML : 3 points
* CSS : 4 points
* Responsive design : 3 points
* JavaScript : 5 points
* Rapport : 2 points
* Qualité du code et respect des consignes : 3 points
* Points bonus : 5 points (voir la 1ere étape)
* Déploiement : 1 point bonus

## Introduction du projet
Le projet de ce semestre se fera de manière progressive et le sujet sera alimenté au fur et à mesure des séances.

L'objectif est de concevoir un site internet présentant une collaboration entre l'Université de Strasbourg et une entreprise/association/musée/outil... (par exemple Starbucks, Unicef, le Louvres, une entreprise locale etc...) en se servant du design des sites de l'Unistra [https://www.unistra.fr/](https://www.unistra.fr/) ou de Math-Info [https://mathinfo.unistra.fr/](https://mathinfo.unistra.fr/) tout en y intégrant les idées et style graphique de la collab.

## 1ere étape - la maquette (**BONUS**)
Vous aurez **jusqu'au vendredi 11 novembre** pour présenter une maquette de votre propre collaboration réalisée par vos soins. Au delà de cette date vous ne bénéficierez plus de points bonus, ni de la possibilité de créer votre propre site.

Un document présentant la maquette sera à rendre au format *.pdf* par mail à l'adresse *antoine.haas@unistra.fr*.

La maquette pourra être faite avec le logiciel (Sketch, Figma, Wix?, etc...) ou encore un dessin (**propre**) à la main. Une fois validée, le développement pourra commencer.

En TP une pré-vérification des maquettes sera faite.

## 2ème étape - le code
La 2ème étape du sujet est de passer de la maquette au site web en codant le HTML, CSS et Javascript. 

Il y a alors deux possibilités :
1. Dans le cas où vous avez réalisé la 1ère étape et que celle-ci a été validée vous pourrez utiliser cette maquette.
2. Dans le cas contraire (non réalisation de la partie bonus ou non accord) vous devrez utiliser une maquette par défaut disponible sur Moodle.

### 1 - Votre propre site
Dans le cas où vous coderez le site de votre propre maquette il est important de respecter les critères de difficultés ci-dessous :

1. Le site doit contenir au minimum quatres pages différentes contenant chacune leur propre design et spécificités tout en conservant la cohérence globale du design.
2. Au minimum une des pages doit contenir une interaction Javascript avec l'utilisateur. Celle-ci devra être décrite et détaillée dans votre rapport.
3. N'oubliez pas d'y inclure le design responsive, une partie de la note portera dessus.
4. En terme de difficulté d'implémentation, votre site devra être à peu près équivalent au site par défaut.

Hormis ces critères, vous êtes libres de réaliser le site que vous souhaitez. La maquette vous servant uniquement d'inspiration vous pouvez bien évidemment vous en éloigner légèrement durant le développement.

### 2 - Site web par défaut
Les captures d'écrans ainsi que les assets du site sont disponibles sur Moodle. Une vidéo détaillant les interactions du site est également disponible.

L'objectif sera ici de coder le site à l'identique en vous servant de tout ce qui vous est mis à disposition.

Le design responsive sur mobile et tablette du site ne vous est pas fourni et est à réaliser par vos propres soins. Une partie de la note portera sur votre interprétation de celui-ci.

## 3ème étape - déploiement
Une petite partie de la note contient, en bonus, la mise en ligne du site. Vous pouvez utiliser divers sites ou méthodes vues en cours :

* netlify
* gitlab pages
* gitlab pages (de l'unistra)
* github pages
* autres

## 4ème étape - celebrate

<img src="https://media3.giphy.com/media/uHhVUeyVpwbEwXuApr/giphy.gif?cid=ecf05e47kxmxjecnr4tv8c521aasgc61c3wdhovqncu3p9bp&amp;rid=giphy.gif&amp;ct=g" alt="See Ya Goodbye GIF by NFL">