---
layout: ../layouts/AirThemeLayout.astro
title: "Informations utiles"
author: "Antoine Haas"
date: "29 août 2022"
---

## Les objectifs de l'UE
* Comprendre les méchanismes du web
* Savoir analyser un site
* Savoir créer un site web statique ou dynamique
* Maîtriser au fur et à mesure les technologies dans leur ordre chronologique pour construire des sites de plus en plus complexes
  * site basique
  * blogs
  * sites vitrines
  * SPA / applications

## Les dates clés
* **Présentation du projet :** Mercredi 28 septembre
* **Contrôle final :** Mercredi 16 novembre
* **Rendu final du projet :** Mercredi 21 décembre