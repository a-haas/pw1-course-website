---
layout: ../layouts/AirThemeLayout.astro
title: "TP 1 - HTML 101"
author: "Antoine Haas"
date: "14 septembre 2022"
---

De la documentation complémentaire et de nombreux exemples sont disponibles sur w3schools.com.
Attention, ne "pompez" pas bêtement les exemples : vous devez être capable d’expliquer votre code.

## Avant toute chose
Configurez en utilisant le document d'aide `Configuration VSCodium` disponible sur Moodle

## Partie 1 : Structuration HTML d'une page web
Choisissez un thème que vous souhaiteriez développer sur votre page web.
L’objet de cette première séance est de proposer une première structuration de cette page.

1. Mettez en forme du contenu à l’aide de titres de différents niveaux, de paragraphes, . . . (pas d’idée
de contenu ? Utiliser du Lorem Ipsum : [http://www.lipsum.com/](http://www.lipsum.com/))

2. Insérer une image dans votre page. Utilisez un lien relatif pour la source; n’oubliez pas de fournir
un texte alternatif. Utilisez un attribut pour donner à votre image une largeur de 200 pixels.

3. Créer un menu de navigation contenant un lien vers une ancre et un lien vers une autre page
contact.html (à laisser vide pour l’instant).

4. Encapsulez les différentes zones de votre page dans des conteneurs (en-tête, menu de navigation,
corps de texte, pied de page). Vous pouvez utiliser les conteneurs `<div>` en définissant des identifiants 
pour chaque zone ou mieux, utiliser les balises sémantiques définies par HTML5 `(<header>`, `<nav>`, . . . )

## Partie 2 : balises d'en-tête

Nous avons vu en cours quelques balises à placer dans l’en-tête d’un document HTML (`<head></head>`).
Certaines permettent de préciser des informations sur le contenu de la page web, d’autres de référencer
des documents, ou encore d’écrire des scripts.



1. Insérer les balises d’en-tête **"description"**, **"keywords"**, **"author"**. Renseigner les attributs **"content"**
avec des valeurs adaptées au contenu de votre page.
Une documentation relativement complète est disponible à cette adresse [https://fr.w3docs.com/apprendre-html/html-tag-meta.html](https://fr.w3docs.com/apprendre-html/html-tag-meta.html)

2. Intégrer dans votre en-tête la balise méta relative au viewport. Quel est son intérêt ?

3. Définissez l’encodage de votre page web avec la balise `<meta charset="..."/>`

4. Quel est l’intérêt d’encoder les documents en utf-8 ?

## Partie 3 : Votre premier blog

Créer un site web en HTML de toute pièce qui devra répondre aux problématiques suivantes :

* Le site doit contenir plusieurs articles (minimum 5) sur un sujet définit (vous pouvez vous baser sur le contenu d'un site que vous appréciez 
ou alors en le remplissant de Lorem Ipsum, comme lors de la première partie)

* Les articles doivent être accessibles depuis n'importe quelle page du site (via une barre de navigation en haut de votre blog ou une map de site à la fin des pages)