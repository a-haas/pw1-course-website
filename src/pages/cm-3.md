---
layout: ../layouts/AirThemeLayout.astro
title: "Design statique avancé et les débuts du JS"
author: "Antoine Haas"
date: "17 octobre 2022"
---

## Responsive web design

### Responsive, qu'est-ce ?
Le responsive est apparu il y a une quinzaine d'année environ, depuis l'arrivée des smartphones et la démocratisation 
de la 3G/4G qui a permis de passer d'un mode de consommation du contenu web traditionnellement sur ordinateur à un mode 
de consommation nomade.

Il désigne donc tout simplement la manière de construire un site web qui peut s'adapter à tout type d'écran et d'appareil.

### Le mobile-first et l'adaptabilité du design
Pour pouvoir rendre son site responsive il est important de penser à l'adaptabilité du site sur chaque catégorie d'écran.

Voici les catégories les plus courantes :

* téléphone
* tablette en mode portrait
* tablette en mode paysage
* ordinateur portable (en fonction de la résolution) ou petits écrans
* grand écran

Des cas d'utilisation supplémentaires peuvent être encore rajoutés tel que téléphone en mode paysage, les écrans ultrawide,
différencier les iphone si l'on cible les produits de la marque Apple, etc...

Il est donc recommandé de couvrir un maximum de cas mais il est nécessaire d'en gérer au moins trois : téléphone, tablette, ordinateur.

Pour se faciliter la vie et diminuer la quantité de travail il est intéressant de travailler en mode *mobile-first*.
Ce mode consiste à concevoir son site sur les écrans les plus contraignants et d'occuper la place laisser vacante pour améliorer et mieux répartir le contenu sur des écrans plus larges.

Il est important de prendre en compte le responsive dès la conception car le mobile et l'écran d'ordinateur fonctionnent de manière opposée.
L'écran mobile est plus long en longueur alors qu'un écran d'ordinateur est plus large que long en règle générale.

Cette différence accentue également le *mobile-first* car on peut se retrouver autrement, avec un site designé en *desktop-first* et du contenu disposé sur la largeur, avec une perte de contenu sur la version mobile ce qui déplara à une grande majorité des utilisateurs. 

Le plus souvent un site en *mobile-first* sera designé sur la longueur avec les informations les unes en dessous des autres. 
Sur un plus grand écran les mêmes infos seront disponibles mais disposées en colonnes pour optimiser l'utilisation de l'espace disponble.

Vous pouvez comparer quelques sites en vous rendant sur cette sélection de sites responsives et en comparant la version mobile et la version desktop : [https://www.awwwards.com/websites/responsive-design/](https://www.awwwards.com/websites/responsive-design/), tel que celui-ci [https://sfo.vc/en/](https://sfo.vc/en/).

### Les breakpoints et media queries ?
Avant l'apparation des media queries, un site web contenait souvant deux designs (un desktop et un mobile, avec des feuilles de style CSS différentes) du même site afin de le rendre accessible aux versions mobiles. *www.site.com* (version *classique*) et *www.m.site.com* (son pendant mobile).

Depuis la démocratisation du CSS3 il est désormais possible d'utilisation les media queries et les breakpoints afin d'appliquer des règles CSS uniquement aux appareils respectant les critères de la media query.

En voici un exemple :

```css
/* Small devices (landscape phones, 576px and up) */
@media (min-width: 576px) { ... }

/* Medium devices (tablets, 768px and up) */
@media (min-width: 768px) { ... }

/* Large devices (desktops, 992px and up) */
@media (min-width: 992px) { ... }

/* Extra large devices (large desktops, 1200px and up) */
@media (min-width: 1200px) { ... }
```

Un descriptif complet des media queries est disponible à cette [https://www.emailonacid.com/blog/article/email-development/emailology_media_queries_demystified_min-width_and_max-width/](https://www.emailonacid.com/blog/article/email-development/emailology_media_queries_demystified_min-width_and_max-width/)

### Les outils pour faire du responsive
Pour pouvoir tester de manière optimale les sites response il faudrait la panoplie complète des écrans disponibles ce qui n'est ni pratique lors de la phase de développement, ni lors de la phase de test, ni financièrement.

Heureusement les navigateurs fournissent tous des devtools pour inspecter le code (nous avons pu l'utiliser à plusieurs reprise lors des précédentes séances) et l'un d'eux est la fonctionnalité responsive.

Le petit bouton ci-dessous (visible dans les devtools) permet d'afficher le mode responsive du navigateur. <img src="/cm-3/responsive-tools-btn.png" alt='Responsive button inside the web browser dev tools' />

<img src="/cm-3/responsive-tools.png" alt='Responsive example inside the web browser dev tools' />

On peut voir sur l'image ci-dessus que l'on peut simuler soit des dimensions personnalisées soit des modèles précis (ici le Samsung Galaxy S8+).

Il est également possible de changer :
* le zoom (de 25% à 200%)
* d'utiliser le mode paysage
* d'activer le throttling et de simuler une mauvaise connexion réseau jusqu'à tester le mode offline

## Les sélecteurs avancés

### Le sélecteur d'attribut
Il est possible de sélectionner des éléments en fonction de leurs attributs spécifiques (hors ID et classe).

```css
/* <div data-type="primary"></div> ou <div data-type="toto"></div>*/
[data-type] {
  color: red;
}
/* <div data-type="primary"></div> */
[data-type='primary'] {
  color: red;
}
```

La documentation complète de ce sélecteur est : [https://developer.mozilla.org/fr/docs/Web/CSS/Attribute_selectors](https://developer.mozilla.org/fr/docs/Web/CSS/Attribute_selectors)

### Les pseudos classes

Documentation complète : [https://web.dev/learn/css/pseudo-classes/](https://web.dev/learn/css/pseudo-classes/)

Les sélecteurs de pseudo classes permettent d'intéragir avec un état précis d'un élément tel que :
* le survol (*:hover*)
* si le clic est maintenu sur l'élément (*:active*)
* si l'élément a été sélectionné (*:focus*)
* si l'élément matche l'ancre de la page (*:target*)
* le premier enfant (*:first-child*)
* le dernier enfant (*:last-child*)
* le ...énième enfant (*:nth-child(...)*)
* si l'élément n'est pas ... (*:not(...)*)

Voici quelques exemples : 

```css
/* Pour indiquer les nuances lors du clic, du survol, au "respos" des boutons */
button {
  background-color: lightblue;
}

button:hover {
  background-color: blue;
  color: white;
}

button:active, button:focus {
  background-color: darkblue;
  color: white;
}

/* Pour changer la couleur de chacune des lignes du tableau (pair gris, impair blanc)*/
table > tbody:nth-child(2n+1) { /* tous les impairs */
  background-color: white;
}

table > tbody:nth-child(2n) { /* tous les pairs */
  background-color: lightgray;
}

/* Pour mettre la première colonne du tableau en gras */
table tr:first-child {
  font-weight: bold;
}
```

### Les pseudos éléments

Documentation complète [https://developer.mozilla.org/fr/docs/Web/CSS/Pseudo-elements](https://developer.mozilla.org/fr/docs/Web/CSS/Pseudo-elements)

Les sélecteurs de pseudos éléments permettent de mettre en forme une partie spécifique d'un élément. Il ne s'agit pas d'un état, contrairement aux pseudos classes, ces pseudos-éléments seront présents physiquement sur la page.

Voici les plus utiles :

* *::after* (apparait derrière l'élément)
* *::before* (apparait avant l'élément)
* *::first-letter* (la première lettre de l'élément)
* *::first-line* (la première ligne de l'élément)
* *::selection* (la sélection de l'élément)

L'écriture des pseudos éléments dans le code CSS est toujours de la forme *<sélecteur>::<pseudo-élément>* 

```css
p {
  color: gray;
}

p::first-line {
  color: blue;
}
```

Dans notre exemple la première ligne du paragraphe *p* sera coloriée en bleu, le reste étant en gris.

## Layout avancés
Depuis CSS3 il est possible d'utiliser des layouts supplémentaires par rapport aux traditionnels *block*, *inline-block* et *inline*. Les plus utilisés sont les *flexbox* et les *grids*.

### Flexbox
Les flexbox permettent de décrire comment les enfants **DIRECTS** de l'élément vont se positionner par rapport à l'espace disponible.
On peut par exemple choisir :
* d'aligner les éléments de gauche à droite
* de droite à gauche
* de haut en bas
* du bas vers le haut
* d'aligner les éléments plutot vers le début du bloc
* vers la fin du bloc
* ou alors au milieu
* ou encore de laisser un maximum d'espace entre chacun des enfants

Voici un court exemple des flexbox permettant de centrer verticalement les deux enfants dans le container tout en les alignant horizontalement à droite et à gauche pour laisser un maximum entre les deux.

```css
.container {
  display: flex;
  justify-content: space-between;
  align-items: center;
  
  width: 50vw;
  height: 50vh;
  margin: auto;
}
```

```html
<div class="container">
  <div>Elément 1</div>
  <button type="button">Elément 2</button>
</div>
```

Un guide complet et détaillé sur l'utilisation des flexbox [https://css-tricks.com/snippets/css/a-guide-to-flexbox/](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

### Grid
Les grids permettent de créer une séparation verticale et horizontale de l'espace disponible sous forme de grille. On peut ainsi définir le nombre de colonne sur la largeur, le nombre de ligne sur la hauteur, les patterns de taille des colonnes et d'espacement, etc...

Il existe également beaucoup de similitude avec les flexbox en terme de syntaxe et de terminologie car fondamentalement très proches.

Bien que très pratiques Les grids sont plus complexes et moins utilisées que les flexbox, c'est pour cela que nous ne nous épancherons pas sur le sujet.

Toutefois un guide complet et détaillé est disponible ici : [https://css-tricks.com/snippets/css/complete-guide-grid/](https://css-tricks.com/snippets/css/complete-guide-grid/)

## Les fonctions
Il est possible depuis CSS3 d'utiliser des fonctions (notamment des fonctions pré-existantes) au sein de ses feuilles de styles.

Nous ne nous attarderons pas dessus mais elle peuvent être utiles au cas par cas. Vous trouverez une description simple et brève de quelques unes d'entres elles à cette adresse [https://web.dev/learn/css/functions/](https://web.dev/learn/css/functions/).

## Un peu de HTML supplémentaire
Lors du premier cours nous avons pu voir ensemble les principales balises HTML. Il reste toutefois une poignée d'entres elles que nous n'avons pas évoquées et qui sont très pratiques.

### Les formulaires
Il est possible en HTML de définir un formulaire à l'aide de la balise `<form></form>`. Très utilisée elle permet l'envoi de données vers un serveur pour qu'il puisse les traiter mais ceci est hors du scope de cette UE.

Néanmoins un formulaire comprend plusieurs champs pour saisir des valeurs et même si nous ne pourrons pas utiliser leur plein potentiel il seront très utiles pour gagner en interactivité au sein de la même page et interagir avec l'utilisateur.

Voici ces tags dits d'input :
* `<input type="text" />` <input type="text" />
* `<input type="date" /> `<input type="date" />
* `<input type="datetime-local" />` <input type="datetime-local" />
* `<input type="color" />` <input type="color" />
* `<input type="button" value="bouton" />` <input type="button" value="bouton" />
* `<input type="file" />` <input type="file" />
* `<input type="month" />` <input type="month" />
* `<select><option>Choix 1</option><option>Choix 2</option></select>` <select><option>Choix 1</option><option>Choix 2</option></select>

Ces tags permettent à l'utilisateur de renseigner les informations nécessaires de manières intuitives et standardiser car ils sont implémentés directement par le navigateur et dans les normes HTML.

De nombreux attributs des inputs existent, principalement pour la validation ou l'aide de saisie à l'utilisateur. 
Le détails de la balise *input* et de ses tags est disponible ici : [https://developer.mozilla.org/fr/docs/Web/HTML/Element/input](https://developer.mozilla.org/fr/docs/Web/HTML/Element/input). De même pour le tag *select* [https://developer.mozilla.org/fr/docs/Web/HTML/Element/select](https://developer.mozilla.org/fr/docs/Web/HTML/Element/select).

Voici un exemple de structuration d'un formulaire classique en HTML, inspiré cette page [https://developer.mozilla.org/fr/docs/Learn/Forms/Your_first_form](https://developer.mozilla.org/fr/docs/Learn/Forms/Your_first_form). 

```html
<form action="/ma-page-de-traitement" method="post">
    <div>
        <label for="name">Nom :</label>
        <input type="text" id="name" name="user_name">
    </div>
    <div>
        <label for="mail">E-mail :</label>
        <input type="email" id="mail" name="user_mail">
    </div>
    <div>
        <button type="submit">Valider</button>
    </div>
</form>
```

Typiquement un *form* est donc de la forme suivante :
1. Ouverture de la balise `<form>`. N.B : Nous nous intéressons pas aux attributs *action* et *method*. Ils concernent l'UE Programmation Web 2 et la communication avec un serveur.
2. De 1 à n élément d'input comprenant un label explication, l'input (de type texte, de type date, de type select, etc...) le tout englober par une *div* pour assurer la séparation.
3. Un bouton de type *submit* pour envoyer le formulaire au serveur (encore une fois, cette partie ne concerne pas cette UE).
4. La fermeture du `</form>`

### Quelques tags utiles
Démonstration complète : [https://www.youtube.com/watch?v=CvwbinzWSgs](https://www.youtube.com/watch?v=CvwbinzWSgs)

**1 - Ajouter de l'auto-complétion à une input de type texte**
```html
<input type="text" placeholder="Input de type texte avec auto-completion" list="usernames" />

<datalist id="autocomplete">
  <option value="autocomplete 1">
  <option value="autocomplete 2">
</datalist>
```

**Attention : les navigateurs proposent également une auto-complétion sur les champs de type input (notamment text) en se basant sur l'attribut name et les anciennes valeurs fournies par l'utilisateur lors des anciennes validation du formulaire.
La documentation de cette autocomplétion est disponible ici : [https://developer.mozilla.org/fr/docs/Web/HTML/Attributes/autocomplete#valeurs](https://developer.mozilla.org/fr/docs/Web/HTML/Attributes/autocomplete#valeurs)** 

**2 - Le tag details**
```html
<details>
  <summary>Example de résumé</summary>
  <p>Partie caché par défaut</p>
</details>
```

Le tag détails est utile pour cacher par défaut une partie du contenu. Cela peut prendre la forme de question (le summary) et la réponse (le reste qui est caché), d'explications longues (cachées par défaut) et un résumé synthétique (summary), etc...

**3 - Faire une pop-up nativement**
```html
<dialog>
  <p>Ma question / mon contenu dans une pop-up</p>
  
  <div>
    <button>Rajouter une interaction avec l'utilisateur ?</button>
  </div>
</dialog>
```

**4 - Rajouter une description à une image/un élément**
```html
<figure>
  <img src="..." alt="Ma description alternative si l'img ne charge pas">
  <figcation>Ma description de l'image qui s'inserera en dessous.</figcaption>
</figure>
```

## Dynamisons notre page avec du Javascript

### JS, les bases
Javascript est un langage de programmation créé en 1995 pour dynamiser et programmer les interactions entre un site web et son utilisateur.

Au fil des années, le langage à évoluer pour devenir un langage multi-usages grâce à *node.js* pour le back-end ou de petits scripts (en mode CLI), *electron* pour des applications *desktop*, *React Native* pour des applis mobiles, etc...
  Du côté du navigateur l'éco-système JS a également énormément évolué en proposant énormément de framework ces dernières tels que *Svelte*, *React JS*, *Angular*, *VueJS*, *Astro*, *Vite*, *Next.js*, *Nuxt.js*, etc... 

### Hello world
```html
<script type="text/javascript">
  alert('Hello, world!');
</script>
```

Comme pour le CSS, il est possible d'importer son code Javascript de plusieurs manières.

1. La première façon, comme ci-dessus, en intégrant le code JS directement dans le HTML et une balise script de type *text/javascript*.
2. Il est aussi possible d'importer un fichier JS en utilisant la balise script et l'attribut *src* : `<script type="text/javascript" src="path/to/my-file.js"></script>`

### Les variables
Pour instancier une variable il est possible d'utiliser les mots-clés *let*, *const* et *var* :
* *let* pour instancier une variable qui pourra être modifiée par la suite
* *const* pour instancier une variable qui ne pourra pas être modifiée
* *var*, la manière la plus ancienne d'instancier une variable. Il est recommandé d'utliser *let* et *const*, hormis en cas de problème de compatibilité (pour des versions de Javascript assez ancienne, pré ES6).

```js
var var1 = "";
let var2 = "";
const var3 = "";
```

### Les types
Il existe en Javascript les types primitifs suivants :

* *boolean* (true et false)
* *null*
* *undefined*
* *number* (-10, -9, -8, ..., 1, 2, 3, ..., 3.14, 3.15, 3.16, ...)
* *string* ("a", "abc", "Lorem Ipsum", ...)

Il existe également le type objet que nous verrons plus tard qui permet de représenter des objets et des dictionnaires au sein du langage.

### Quelques fonctions de bases
* *console.log(...)* pour print dans la console javascript
* *console.table(...)*, pareil que pour *log()* mais sous forme de tableau (très pratique pour afficher un dictionnaire JSON ou un objet JS)
* *alert("...")* pour éméttre une "alerte" sous forme de pop-up
* *prompt("...") pour ouvrir une fenêtre de saisie pour l'utilisateur*

**Les fonctions *alert* et *prompt* bien que pratique lors du développement et de tests ne sont en réalité que peu utilisées car très intrusives pour l'expérience utilisateur !**

**De même pour les fonctions _console.*_ elles ne sont pas utilisées en production mais pour le débuggage.**

### Récupérer un élément et le modifier
```html
<html>
  <body>
    <div id="el1" class="elems"></div>
    <div class="elems"></div>
    <div class="elems"></div>
    <span class="elems"></span>
    
    <script type="text/javascript">
      // get an HTML elem by ID
      const el1 = document.getElementById('el1')
      // get HTML elems by classname
      const elems = document.getElementsByClassName('elems')
      // get HTML elem by tagname
      const divs = document.getElementsByTagName('div')
      
      // get HTML elem by CSS selector
      const alternative = document.querySelector('#el1')
      const alternative2 = document.querySelector('.elems')
      
      // add or remove a class
      alternative.classList.add("add-a-new-classe")
      alternative.classList.remove("add-a-new-classe")

    </script>
  </body>
</html>
```
Nous avons vu uniquement un petit nombre de fonctions et propriétés permettant de modifier un élémént, mais un grand nombre d'autres fonctions sont disponibles dans la documentation à cette adresse [https://developer.mozilla.org/en-US/docs/Web/API/Element](https://developer.mozilla.org/en-US/docs/Web/API/Element).

### Les évènements
```html
<html>
  <body>
    <button id="el1" class="btn" onclick="console.log('clicked btn 1')"></button>
    <button id="el2" class="btn"></button>
    
    <script type="text/javascript">
      const el2 = document.querySelector('#el1')
      el2.on("click", () => { alert("Clicked btn 2!"); }      
    </script>
  </body>
</html>
```

Il existe un grand nombre d'évènements en Javascript. Pour cette séance, nous ne nous concentrerons uniquement sur l'évènement *click* qui se produit, comme son nom l'indique, au clic de l'élément associé (le plus souvent un bouton ou un lien).

Il est possible de programmer un évènement JS via deux méthodes :
1. Par l'attribut *onclick* dans lequel on ajoute le code JS à exécuter
2. Via la fonction .addEventListener("<le type d'évènement>", `() => {/* la fonction contentant le code à exécuter*/}`)

La 2eme méthode, passant par la fonction *addEventListener* et utilisant une **fonction anonyme** est traditionnelement la plus utilisée.
Pour déclarer une fonction anonyme (e.g. qui n'a pas de nom et qui n'est utilisable qu'à l'endroit où elle est déclarée) il deux façons de faire :

1. `() => { /*le code js */ }`
2. `function() { /* le code js */}`