---
layout: ../layouts/AirThemeLayout.astro
title: "Production-ready website"
author: "Antoine Haas"
date: "30 novembre 2022"
---

## La charte graphique
La charte graphique est l'essence même d'un site web qui lui permet d'avoir une identité unique et reconaissable parmi tous les autres. Il s'agit en fait d'un ensemble de règles qui doivent être absolument respectées ou bien qui peuvent également servir d'inspiration. 

Celle-ci est souvent composée de plusieurs éléments notoires dont :
* la typographie
* les couleurs
* les icônes
* les principaux composants

Evidemment d'autres éléments peuvent être inclus dans cette charte, tels que le logo, la/les mascottes, le design des images, le ton du contenu et encore bien d'autres choses.

### La police de caractères
La police de caractères d'un site est d'une importance capitale car elle permet à elle seule de dicter le ton utiliser par le site. Certaines entreprises ou entités utilisent leur propre police telle que *Marianne* [https://www.gouvernement.fr/charte/charte-graphique-les-fondamentaux/la-typographie](https://www.gouvernement.fr/charte/charte-graphique-les-fondamentaux/la-typographie) qui a été créé spécifiquement pour l'utilisation des sites du gouvernement français.

Pour la choisir, la première étape est de choisir entre le serif ou le sans-serif (avec ou sans empattement des caratères).
Une police serif permettra d'insuffler la solidité, la sobriété et la constance liée à son utilisation depuis plus d'un siècle dans les journaux et autres institutions.
La police sans-serif exprime plutôt la modernité, le minimalisme et le renouveau.
En réalité, ces deux familles ne sont pas contradictoires et peuvent très bien être utilisées ensemble, en mettant les titres en sans-serif et le corps du texte en serif, par exemple.

Toutefois il est important de rappeler qu'une limite de 3 polices est recommandée par site afin de ne pas perdre l'utilisateur. Toujours pour la même raison, il n'est conseillé pas d'utiliser de police cursive (Comic Sans MS, etc,...).

Pour en savoir plus : [https://fonts.google.com/knowledge](https://fonts.google.com/knowledge)

### La palette de couleur
Tout comme la typographie, la palette de couleur utilisée par le site permet de véhiculer diverse émotion :

<img src='/cm-5/color-palette.png' alt="color palette image" />

*Source : [https://londonimageinstitute.com/how-to-empower-yourself-with-color-psychology/](https://londonimageinstitute.com/how-to-empower-yourself-with-color-psychology/)*

Il est donc important de choisir les couleurs principales du sites ainsi que les couleurs secondaires. De manière générale, 5 couleurs au total (2 principales, 3 secondaires par exemple) forment un bon équilibre.

Si vous n'êtes pas expert en colorimétrie, vous pouvez vous servir de sites pour générer vos palettes, tels que :
* [https://color.adobe.com/fr/create/color-wheel](https://color.adobe.com/fr/create/color-wheel)
* [https://colorhunt.co/](https://colorhunt.co/)
* [https://coolors.co/](https://coolors.co/)

Si vous souhaitez une teinte relativement précise vous pouvez vous inspirer des couleurs de référence Pantone qui sont cataloguées par l'entreprise Pantone. Vous pourrez les trouvez par exemple ici : [https://www.toutes-les-couleurs.com/nuancier-pantone.php](https://www.toutes-les-couleurs.com/nuancier-pantone.php).

### Les icônes
Depuis plusieurs années, et notamment avec l'arrivée des smartphones, il est de plus en plus récurrent de trouver des icônes sur les sites afin de convoyer rapidement une idée et pouvoir également économiser de l'espace (surtout sur mobile).

Elles sont donc devenus incontournables et il est donc nécessaire de savoir et pouvoir les utiliser.

Pour ce faire il existe principalement quatres méthodes :
* Les créer soi-même
* Utiliser une police de caractère incluant les icônes tel que le projet Nerd Font [https://github.com/ryanoasis/nerd-fonts](https://github.com/ryanoasis/nerd-fonts)
* Intégrer des SVG d'icônes [https://heroicons.dev/](https://heroicons.dev/)
* Intégrer une libraire CSS incluant des sets d'icônes [https://fontawesome.com/icons](https://fontawesome.com/icons) ou [https://feathericons.com/](https://feathericons.com/)

### Les composants
Pour se simplifier la vie il est possible de designer plusieurs composants qui pourront être réutiliser au sein du site. Ces composants sont un ensemble de HTML et CSS qui pourront être simplement c/c sur vos différentes pages et dont le coeur du design sera contenu dans votre code CSS. Ces composants peuvent être également composés de variantes (couleurs, ...) et peuvent être imbriqués entre eux.

Voici quelques exemples de composants :
* boutons (primary, secondary, warning, error, ...)
* pagination de tables
* composant de type *card* avec un titre, une description, une image et un bouton cta
* du texte avec une icône
* photo de profil
* etc...

```html
<button class="btn btn-primary">...</button>
```

```css
.btn {
  display: inline-flex;
  flex-shrink: 0;
  cursor: pointer;
  user-select: none;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
  border-color: transparent;
  text-align: center;
}

.btn-primary {
  color: white;
  color: purple;
}

.btn-warning {
  color: black;
  background-color: orange;
}

.btn-secondary {
  /*... */
}
```

### Librairies
Nous venons de voir que les composants sont très pratiques et un très bon moyen de designer son site web. Cependant concevoir tous les composants est très chronophage et n'est que viable pour les projets d'envergures.

Il existe tout de même un moyen de se simplifier la vie en utilisant des librairies de composants pré-construits et customisables. Il en existe de très nombreuses :
* [https://daisyui.com/](https://daisyui.com/)
* [https://getbootstrap.com/](https://getbootstrap.com/)
* [https://get.foundation/](https://get.foundation/)
* [https://tailwindcss.com/](https://tailwindcss.com/)

Il existe également de nombreuses autres librairies permettant de :
* créer des sites minimalistes
* d'importer des composants
* d'importer des icônes
* d'importer des classes CSS utilitaires
* autres, etc...

Pour installer une librairie CSS il suffit la plupart du temps de suivre les indications données par celle-ci.

### Pour s'inspirer
Pour créer votre propre charte graphique vous pouvez vous inspirer des chartes disponibles en ligne ci-dessous :
* [https://langagevisuel.unistra.fr/](https://langagevisuel.unistra.fr/)
* [https://www.gouvernement.fr/marque-Etat](https://www.gouvernement.fr/marque-Etat)

Il est aussi possible d'utiliser Dribbble [https://dribbble.com/](https://dribbble.com/) qui possède une grande communauté et un gros panels de designs proposés par celle-ci.

## Javascript avancé

### Les requêtes asynchrones
Lors des précédents cours nous avons pu voir la base de Javascript avec la manipulation du DOM, les évènements, les boucles, les conditions, les objets, etc...

Il nous reste désormais une fonctionnalité principale de Javascript que nous n'avons pas encore évoquer et qui permet de télécharger et d'envoyer des données de manière asynchrone et totalement transparente pour l'utilisateur. Nous en aurons une certaine utilité, mais son plein potentiel pourra être utilisé dans l'UE Programmation Web 2 en communiquant en asynchrone avec le back-end.

La base de ces requêtes asynchrones utilisent l'objet *XMLHttpRequest* et est désigné de manière générale sous le terme AJAX.

```js
function loadDoc(url) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      // on succes...
    }
  };
  xhttp.open("GET", url, true);
  xhttp.send();
}
```

Depuis ES6 il est possible d'utiliser l'API *fetch* qui est bien plus simple et lisible.

```js
fetch('http://example.com/movies.json')
  .then((response) => response.json())
  .then((data) => console.log(data))
  .catch(() => { /* handle errors */});
```

### Les librairies
Pour se simplifier la vie en Javascript il est tout à fait possible, comme en CSS, d'utiliser des librairies regroupant les fonctionnalités dont le site à besoin en facilitant son implémentation, améliorant la lisibilité tout en y intégrant la documentation.

Les cas d'utilisations sont nombreux, on peut citer notamment :
* les librairies de requêtage, on peut citer par exemple *Axios* qui simplifie encore davantage les requêtes AJAX
* les librairies de visualisation de données, par exemple *Chart.js* qui permet d'afficher des graphiques, etc...
* les librairies de manipulation de données comme *D3.js*
* les librairies de manipulation du DOM comme *jquery.js*
* les librairies de modélisation 3D comme *three.js* basé sur *webgl*

Néanmoins même si les lib permettent de gagner énormément de temps de développement il reste nécessaire d'assurer leur mise à jour afin de s'assurer d'avoir les dernières fonctionnalités et de ne inclure de failles de sécurité dans son site.

Heureusement des gestionnaires de packages existent donc Yarn ou NPM pour faciliter le suivi de celles-ci.

### Les frameworks

Voici quelques un des frameworks JS les plus en vogue ces dernières années :  

* Alpine.js
* React.js
* Vue.js
* Svelte.js
* Angular.js
* ...

Il existe également de nombreux framework permettant de générer des sites web dits statiques, qui sont en fait des sites uniquement front-end. En voici quelqu'uns :

* Astro.js
* Next.js
* Nuxt.js
* Gatsby.js
* ...

Il en existe également bien d'autres hors de l'éco-système Javascript, tel que Hugo ou encore Jekyll.

## Pour la production

### Minification
Lors d'un déploiement d'une application en production il est important de prendre en compte la taille de la page envoyée. On peut compter principalement trois types de fichiers pouvant avoir une taille conséquente : 
* les fichiers CSS
* les fichiers JS
* les images

Il existe plusieurs niveau d'optimisation de la minification des fichiers CSS et JS :
1. Enlever les sauts de lignes, les tabulations, les commentaires, les espaces en trop, etc...
2. Réunir plusieurs fichiers en un seul
3. Optimiser le code (équivalent de l'option -o de GCC pour du code C par exemple) 

La plupart du temps lorsque vous utilisez un framework celui-ci ce chargera de construire le build de production pour vous.
Il est également possible de le faire manuellement en utilisant des sites tels que [https://minify.js.org/](https://minify.js.org/).

### Accessibilité
L'accessibilité est une notion essentielle et critique d'un site web pour diverses raisons telles que :
* inclure dans sa communication un maximum de personnes
* améliorer son référencement
* améliorer l'expérience utilisateur en générale

Vous trouverez une introduction détaillée via l'article suivant : 
[https://blog.hubspot.com/website/web-accessibility](https://blog.hubspot.com/website/web-accessibility)

Pour tester l'accessibilité et les performances de son site vous pouvez utiliser la solution lighthouse de Google directement intégrée dans Google Chrome : [https://developer.chrome.com/docs/lighthouse/overview](https://developer.chrome.com/docs/lighthouse/overview)